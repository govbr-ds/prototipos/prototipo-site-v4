const itemsMenu = [
  {
    label: 'Página Inicial',
    link: 'home',
    icon: 'fas fa-home',
  },
  {
    label: 'Introdução',
    icon: 'fas fa-exclamation-circle',
    filhos: [
      {
        label: 'Sobre o Design System',
        link: 'javascript: void(0)',
      },
      {
        label: 'Princípios do Design System',
        link: 'javascript: void(0)',
      },
      {
        label: 'Comunidade',
        link: 'javascript: void(0)',
      },
    ],
  },
  {
    label: 'Como Comecar',
    icon: 'fas fa-play',
    filhos: [
      {
        label: 'Guia de Instalação',
        link: 'como-comecar',
      },
      {
        label: 'Padrão Mínimo',
        link: 'javascript: void(0)',
      },
      {
        label: 'Prototipando com DS',
        link: 'javascript: void(0)',
      },
    ],
  },
  {
    label: 'Diretrizes',
    icon: 'fas fa-file-alt',
    filhos: [
      {
        label: 'Fundamentos Visuais',
        link: 'javascript: void(0)',
        filhos: [
          {
            label: 'Introdução',
            link: 'fundamentos',
            hightlight: true,
          },
          {
            label: 'Cores',
            link: 'cores',
          },
          {
            label: 'Elevação',
            link: 'elevacao',
          },
          {
            label: 'Espaçamento',
            link: 'espacamento',
          },
          {
            label: 'Estados',
            link: 'estados',
          },
          {
            label: 'Grids',
            link: 'grids',
          },
          {
            label: 'Iconografia',
            link: 'iconografia',
          },
          {
            label: 'Ilustração',
            link: 'ilustracao',
          },
          {
            label: 'Movimento',
            link: 'movimento',
          },
          {
            label: 'Superfície',
            link: 'superficie',
          },
          {
            label: 'Tipografia',
            link: 'tipografia',
          },
        ],
      },
      {
        label: 'Componentes',
        link: 'javascript: void(0)',
        filhos: [
          {
            label: 'Introdução',
            link: 'javascript: void(0)',
            hightlight: true,
          },
          {
            label: 'Avatar',
            link: 'javascript: void(0)',
          },
          {
            label: 'Breadcrumb',
            link: 'javascript: void(0)',
          },
          {
            label: 'Button',
            link: 'javascript: void(0)',
          },
          {
            label: 'Card',
            link: 'javascript: void(0)',
          },
          {
            label: 'Carousel',
            link: 'javascript: void(0)',
          },
          {
            label: 'Checkbox',
            link: 'javascript: void(0)',
          },
          {
            label: 'Cookiebar',
            link: 'javascript: void(0)',
          },
          {
            label: 'DateTimePicker',
            link: 'javascript: void(0)',
          },
          {
            label: 'Divider',
            link: 'javascript: void(0)',
          },
          {
            label: 'Footer',
            link: 'javascript: void(0)',
          },
          {
            label: 'Header',
            link: 'javascript: void(0)',
          },
          {
            label: 'Input',
            link: 'javascript: void(0)',
          },
          {
            label: 'Item',
            link: 'javascript: void(0)',
          },
          {
            label: 'List',
            link: 'javascript: void(0)',
          },
          {
            label: 'Loading',
            link: 'javascript: void(0)',
          },
          {
            label: 'Magic Button',
            link: 'javascript: void(0)',
          },
          {
            label: 'Menu',
            link: 'javascript: void(0)',
          },
          {
            label: 'Message',
            link: 'javascript: void(0)',
          },
          {
            label: 'Modal',
            link: 'javascript: void(0)',
          },
          {
            label: 'Notification',
            link: 'javascript: void(0)',
          },
          {
            label: 'Pagination',
            link: 'javascript: void(0)',
          },
          {
            label: 'Radio',
            link: 'javascript: void(0)',
          },
          {
            label: 'Select',
            link: 'javascript: void(0)',
          },
          {
            label: 'Sign-In',
            link: 'javascript: void(0)',
          },
          {
            label: 'Skip Link',
            link: 'javascript: void(0)',
          },
          {
            label: 'Step',
            link: 'javascript: void(0)',
          },
          {
            label: 'Switch',
            link: 'javascript: void(0)',
          },
          {
            label: 'Tab',
            link: 'javascript: void(0)',
          },
          {
            label: 'Table',
            link: 'javascript: void(0)',
          },
          {
            label: 'Tag',
            link: 'javascript: void(0)',
          },
          {
            label: 'Textarea',
            link: 'javascript: void(0)',
          },
          {
            label: 'Upload',
            link: 'javascript: void(0)',
          },
          {
            label: 'Wizard',
            link: 'javascript: void(0)',
          },
        ],
      },
      {
        label: 'Padrões',
        link: 'javascript: void(0)',
        filhos: [
          {
            label: 'Introdução',
            link: 'javascript: void(0)',
            hightlight: true,
          },
          {
            label: 'Ajuda e Comunicação',
            link: 'javascript: void(0)',
          },
          {
            label: 'Base',
            link: 'javascript: void(0)',
          },
          {
            label: 'Collapse',
            link: 'javascript: void(0)',
          },
          {
            label: 'Content Overflow',
            link: 'javascript: void(0)',
          },
          {
            label: 'Densidade',
            link: 'javascript: void(0)',
          },
          {
            label: 'Desenvolvimento Mobile',
            link: 'javascript: void(0)',
          },
          {
            label: 'Dropdown',
            link: 'javascript: void(0)',
          },
          {
            label: 'Empty States',
            link: 'javascript: void(0)',
          },
          {
            label: 'Erro',
            link: 'javascript: void(0)',
          },
          {
            label: 'Formulário',
            link: 'javascript: void(0)',
          },
          {
            label: 'Gráfico',
            link: 'javascript: void(0)',
          },
          {
            label: 'Microcopy',
            link: 'javascript: void(0)',
          },
          {
            label: 'Navegação',
            link: 'javascript: void(0)',
          },
          {
            label: 'Onboarding',
            link: 'javascript: void(0)',
          },
          {
            label: 'UX Writing',
            link: 'javascript: void(0)',
          },
        ],
      },
    ],
  },
  {
    label: 'Downloads',
    icon: 'fas fa-download',
    filhos: [
      {
        label: 'Packs / Assets',
        link: 'javascript: void(0)',
      },
      {
        label: 'Templates',
        link: 'javascript: void(0)',
      },
      {
        label: 'UI Kits',
        link: 'javascript: void(0)',
      },
    ],
  },
  {
    label: 'Suporte e Feedback',
    icon: 'fas fa-life-ring',
    filhos: [
      {
        label: 'Avalie o Design System',
        link: 'javascript: void(0)',
      },
      {
        label: 'Fale Conosco',
        link: 'javascript: void(0)',
      },
      {
        label: 'FAQ',
        link: 'javascript: void(0)',
      },
      {
        label: 'Tutoriais',
        link: 'javascript: void(0)',
      },
    ],
  },
  {
    label: 'Atualizações',
    icon: 'fas fa-clock',
    filhos: [
      {
        label: 'O que há de novo?',
        link: 'javascript: void(0)',
      },
      {
        label: 'Release Notes',
        link: 'javascript: void(0)',
      },
    ],
  },
]

const itemsFooterMenu = [
  {
    label: 'Comunidade (Discord)',
    link: 'https://discord.gg/NkaVZERAT7',
    divider: false,
  },
  {
    label: 'Repositório (GitLab)',
    link: 'https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core',
    divider: false,
  },
  {
    label: 'Web Components',
    link: 'https://gitlab.com/govbr-ds/wbc/govbr-ds-wbc',
    divider: false,
  },
  {
    label: 'Wiki',
    link: 'https://gov.br/ds/wiki/',
    divider: true,
  },
  {
    label: 'Feedback',
    link: 'javascript: void(0)',
    divider: false,
  },
  {
    label: 'Pesquisa Analítica',
    link: 'javascript: void(0)',
    divider: false,
  },
]

export { itemsFooterMenu, itemsMenu }
