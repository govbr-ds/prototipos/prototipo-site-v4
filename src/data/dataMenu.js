const baseUrl = process.env.PUBLIC_URL
const itemsMenu = [
  {
    label: 'Página Inicial',
    link: `${baseUrl}/home`,
    icon: 'fas fa-home',
  },
  {
    label: 'Introdução',
    icon: 'fas fa-exclamation-circle',
    filhos: [
      {
        label: 'Sobre',
        link: `${baseUrl}/introducao/sobre`,
      },
      {
        label: 'Princípios e Valores',
        link: `${baseUrl}/introducao/principios`,
      },
      {
        label: 'Como Começar',
        link: `${baseUrl}/introducao/como-comecar`,
      },
      {
        label: 'Fluxo de Criação',
        link: `${baseUrl}/introducao/fluxo-criacao`,
      },
      {
        label: 'Fluxo de Desenvolvimento',
        link: `${baseUrl}/introducao/fluxo-desenvolvimento`,
      },
    ],
  },
  {
    label: 'Diretrizes',
    icon: 'fas fa-file-alt',
    filhos: [
      {
        label: 'Fundamentos Visuais',
        icon: 'fas fa-palette',
        filhos: [
          {
            label: 'Introdução',
            link: `${baseUrl}/fundamentos`,
            hightlight: true,
            disabled: false,
          },
          {
            label: 'Cor',
            link: `${baseUrl}/fundamentos/cor`,
            disabled: false,
          },
          {
            label: 'Design Token',
            link: `${baseUrl}/fundamentos/design-token`,
            disabled: false,
          },
          {
            label: 'Elevação',
            link: `${baseUrl}/fundamentos/elevacao`,
            disabled: false,
          },
          {
            label: 'Espaçamento',
            link: `${baseUrl}/fundamentos/espacamento`,
            disabled: false,
          },
          {
            label: 'Estado',
            link: `${baseUrl}/fundamentos/estado`,
            disabled: false,
          },
          {
            label: 'Grid',
            link: `${baseUrl}/fundamentos/grid`,
            disabled: false,
          },
          {
            label: 'Iconografia',
            link: `${baseUrl}/fundamentos/iconografia`,
            disabled: false,
          },
          {
            label: 'Ilustração',
            link: `${baseUrl}/fundamentos/ilustracao`,
            disabled: false,
          },
          {
            label: 'Movimento',
            link: `${baseUrl}/fundamentos/movimento`,
            disabled: false,
          },
          {
            label: 'Superfície',
            link: `${baseUrl}/fundamentos/superficie`,
            disabled: false,
          },
          {
            label: 'Tipografia',
            link: `${baseUrl}/fundamentos/tipografia`,
            disabled: false,
          },
        ],
      },
      {
        label: 'Padrões',
        icon: 'fas fa-award',
        filhos: [
          {
            label: 'Introdução',
            link: `${baseUrl}/padroes`,
            hightlight: true,
            disabled: false,
          },
          {
            label: 'Collapse',
            link: `${baseUrl}/padroes/collapse`,
            disabled: false
          },
          {
            label: 'Content Overflow',
            link: `${baseUrl}/padroes/content-overflow`,
            disabled: false
          },
          {
            label: 'Densidade',
            link: `${baseUrl}/padroes/densidade`,
            disabled: false,
          },
          {
            label: 'Dropdown',
            link: `${baseUrl}/padroes/dropdown`,
            disabled: false
          },
          {
            label: 'Formulário',
            link: `${baseUrl}/padroes/formulario`,
            disabled: false
          },
          {
            label: 'Internacionalização',
            link: `${baseUrl}/padroes/internacionalizacao`,
            disabled: false,
          },
          {
            label: 'Skeleton Screen',
            link: `${baseUrl}/padroes/skeleton`,
            disabled: false,
          },
          {
            label: 'Tema',
            link: `${baseUrl}/padroes/tema`,
            disabled: false,
          },
          {
            label: 'Writing',
            link: `${baseUrl}/padroes/writing`,
            disabled: false,
          }
        ],
      },
      {
        label: 'Componentes',
        icon: 'fas fa-cubes',
        filhos: [
          {
            label: 'Introdução',
            link: `${baseUrl}/componentes`,
            hightlight: true,
            disabled: false,
          },
          {
            label: 'Button',
            link: `${baseUrl}/componentes/button`,
            disabled: false,
          },
          {
            label: 'Divider',
            link: `${baseUrl}/componentes/divider`,
            disabled: false,
          },
          {
            label: 'Form-Label',
            link: `${baseUrl}/componentes/form-label`,
            disabled: false,
          },
          {
            label: 'Icon',
            link: `${baseUrl}/componentes/icon`,
            disabled: false,
          },
          {
            label: 'Message',
            link: `${baseUrl}/componentes/message`,
            disabled: false,
          },
          {
            label: 'Text',
            link: `${baseUrl}/componentes/text`,
            disabled: false,
          }
        ],
      },
    ]
  }
]

const itemsFooterMenu = [
  {
    label: 'Comunidade (Discord)',
    link: 'https://discord.gg/NkaVZERAT7',
    divider: false,
  },
  {
    label: 'Repositório (GitLab)',
    link: 'https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core',
    divider: false,
  },
  {
    label: 'Web Components',
    link: 'https://gitlab.com/govbr-ds/wbc/govbr-ds-wbc',
    divider: false,
  },
  {
    label: 'Wiki',
    link: 'https://gov.br/ds/wiki/',
    divider: true,
  },
  {
    label: 'Feedback',
    link: 'https://forms.office.com/r/e0C589jcVv',
    divider: false,
  },
  {
    label: 'Pesquisa Analítica',
    link: 'https://forms.office.com/r/skcRJVX8ua',
    divider: false,
  },
]

export { itemsFooterMenu, itemsMenu }
