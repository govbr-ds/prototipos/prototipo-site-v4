import { v4 as uuidv4 } from 'uuid'

export const dataCarousel = [
  {
    id: uuidv4(),
    title: 'Participe da Comunidade',
    description:
      'Estamos em constante busca de aperfeiçoamento. Entretanto, entendemos que uma evolução com qualidade só é possível com a colaboração de todos.',
    image: 'img/comunidade.png',
    bg: 'bg1',
    link: 'https://discord.com/invite/NkaVZERAT7',
  },
  {
    id: uuidv4(),
    title: 'Conheça os Web Components',
    description:
      'Os Web Components agnósticos oferecem uma abordagem flexível e eficiente para a criação de  aplicações web mais modulares, intercambiáveis e sustentáveis.',
    image: 'img/img-webcomponents.png',
    bg: 'bg2',
    link: 'https://gitlab.com/govbr-ds/wbc/govbr-ds-wbc',
  },
  {
    id: uuidv4(),
    title: 'Design System Acessível',
    description:
      'Com padrões de design inclusivos, o Design System GovBr oferece às pessoas, independentemente de suas habilidades ou necessidades especiais, a possibilidade de interagir de forma eficaz com os produtos e serviços governamentais.',
    image: '',
    bg: 'bg3',
    link: '',
  },
]
