const baseUrl = process.env.PUBLIC_URL
const cardsFundamentals = [
  {
    title: 'Cor',
    description:
      'Confira nossas novas paletas de cores e o conceito de cores funcionais que visam trazer mais facilidade no desenvolvimento de temas.',
    image: 'img/img-cores.png',
    link: `${baseUrl}/fundamentos/cor`,
    highlight: false,
  },
  {
    title: 'Design Token',
    description:
      'Design tokens são dados que representam decisões de design, garantindo consistência nos produtos. Eles definem valores de estilo, como cores, tipografias e espaçamentos, a partir de uma origem confiável, substituindo valores codificados.',
    image: 'img/img-token.png',
    link: `${baseUrl}/fundamentos/design-token`,
    highlight: true,
  },
  {
    title: 'Elevação',
    description:
      'Elevação é um aspecto importante na organização do layout pois permite que o usuário entenda a relação hierárquica entre diferentes elementos na tela.',
    image: 'img/img-elevacao.png',
    link: `${baseUrl}/fundamentos/elevacao`,
    highlight: false,
  },
  {
    title: 'Espaçamento',
    description: 'Espaçamento é a distância entre um elemento a outro, representado por alguma unidade métrica.',
    image: 'img/img-espacamento.png',
    link: `${baseUrl}/fundamentos/espacamento`,
    highlight: false,
  },
  {
    title: 'Estado',
    description:
      'Estados são representações visuais para feedbacks interativos ou informacionais relacionados aos elementos na interface.',
    image: 'img/img-estado.png',
    link: `${baseUrl}/fundamentos/estado`,
    highlight: false,
  },
  {
    title: 'Grid',
    description:
      'Crie protótipos consistentes e estruturados utilizando corretamente o sistema de grid que desenvolvemos para facilitar o seu trabalho.',
    image: 'img/img-grid.png',
    link: `${baseUrl}/fundamentos/grid`,
    highlight: false,
  },
  {
    title: 'Iconografia',
    description:
      'Os ícones são representações gráficas que podem simbolizar uma ação, comunicar mudança de estados ou apenas ser uma representação gráfica.',
    image: 'img/img-iconografia.png',
    link: `${baseUrl}/fundamentos/iconografia`,
    highlight: false,
  },
  {
    title: 'Ilustração',
    description: 'Ilustração é uma imagem que comunica um estado de forma mais amigável.',
    image: 'img/img-ilustracao.png',
    link: `${baseUrl}/fundamentos/ilustracao`,
    highlight: false,
  },
  {
    title: 'Movimento',
    description:
      'As macro ou micro animações podem enriquecer a interface deixandoas mais interativas e interessantes. Use esse recursos ao seu favor para não errar a mão.',
    image: 'img/img-movimento.png',
    link: `${baseUrl}/fundamentos/movimento`,
    highlight: false,
  },
  {
    title: 'Superfície',
    description: 'Superfície é qualquer forma indivisível que pode conter um ou mais elementos.',
    image: 'img/img-superficie.png',
    link: `${baseUrl}/fundamentos/superficie`,
    highlight: false,
  },
  {
    title: 'Tipografia',
    description:
      'Para a apresentação dos elementos textuais do Design System, somente uma família de fonte é utilizada: a Rawline.',
    image: 'img/img-tipografia.png',
    link: `${baseUrl}/fundamentos/tipografia`,
    highlight: false,
  },
]

const cardsPattern = [
  {
    title: 'Collapse',
    description: 'Permite ocultar e revelar conteúdos, ajudando usuários a expandir ou contrair seções. Isso economiza espaço na tela e organiza informações, melhorando a experiência do usuário com interfaces mais limpas e interativas.',
    image: 'img/img-collapse.png',
    link: `${baseUrl}/padroes/collapse`,
    highlight: false,
  },
  {
    title: 'Content Overflow',
    description: 'Oferece técnicas e recursos para otimizar a organização na tela, trabalhando conteúdos ocultos na interface. Use essas técnicas para reduzir a sobrecarga de informações e melhorar a leitura e comunicação com o usuário.',
    image: 'img/img-content-overflow.png',
    link: `${baseUrl}/padroes/content-overflow`,
    highlight: false,
  },
  {
    title: 'Densidade',
    description: 'Densidade é a especificação dos espaçamentos vazios que compõem a tela ou os componentes de interface, e pode ser ajustada dentro de uma escala de intensidade, de forma a atender diferentes objetivos no projeto de layout ou implementação.',
    image: 'img/img-densidade.png',
    link: `${baseUrl}/padroes/densidade`,
    highlight: false,
  },
  {
    title: 'Dropdown',
    description: 'O padrão Dropdown exibe temporariamente um conteúdo flutuante sobre a tela, acionado por um elemento. Facilita a transmissão de mensagens que requerem atenção especial, como avisos de feedback e informações fornecidas pelo usuário, entre outras, sem modificar a grid ou o tamanho dos elementos presentes na interface.',
    image: 'img/img-dropdown.png',
    link: `${baseUrl}/padroes/dropdown`,
    highlight: false,
  },
  {
    title: 'Formulário',
    description: 'Formulários são conjuntos de controles de entrada relacionados que permitem aos usuários fornecer dados ou configurar opções. Eles podem variar de simples a complexos e ser exibidos em diferentes formatos, como páginas dedicadas, painéis laterais ou diálogos, dependendo do contexto e das necessidades do usuário.',
    image: 'img/img-formulario.png',
    link: `${baseUrl}/padroes/formulario`,
    highlight: false,
  },
  {
    title: 'Internacionalização',
    description: 'Internacionalização (i18n) é o processo de projetar produtos para fácil adaptação a diferentes idiomas e regiões, sem grandes mudanças no código. Isso garante que serviços públicos sejam acessíveis a todos, independentemente da língua ou localização.',
    image: 'img/img-internacionalizacao.png',
    link: `${baseUrl}/padroes/internacionalizacao`,
    highlight: false,
  },
  {
    title: 'Skeleton Screen',
    description: 'Skeleton screen é uma técnica de design onde uma versão simples da interface aparece enquanto o conteúdo carrega. Isso mantém o usuário engajado, reduz a percepção de espera e melhora a experiência ao mostrar progresso imediato em vez de telas vazias.',
    image: 'img/img-skeleton.png',
    link: `${baseUrl}/padroes/skeleton`,
    highlight: true,
  },
  {
    title: 'Tema',
    description: 'Ajuste layouts mantendo os elementos essenciais que os distinguem como partes de um sistema coeso. Encontre aqui os direcionamentos necessários para a elaboração de temas que estejam em conformidade com os princípios do Design System.',
    image: 'img/img-tema.png',
    link: `${baseUrl}/padroes/tema`,
    highlight: false,
  },
  {
    title: 'Writing',
    description: 'Writing foca em clareza e inclusão, assegurando que todos compreendam os textos lidos. Utilize este guia para melhorar a experiência do usuário, promover eficiência e construir uma relação de transparência e democracia entre o governo e o povo brasileiro.',
    image: 'img/img-writing.png',
    link: `${baseUrl}/padroes/writing`,
    highlight: false,
  }
]

const cardsComponents = [
  {
    title: 'Button',
    description: 'São elementos interativos que permitem aos usuários executar ações ou tomar decisões. São projetados para serem facilmente reconhecíveis e acessíveis, garantindo consistência visual e funcional em toda a aplicação.',
    image: 'img/img-button.png',
    link: `${baseUrl}/componentes/button`,
    highlight: false,
  },
  {
    title: 'Divider',
    description: 'O Divider é um elemento visual utilizado em interfaces digitais para separar seções ou conteúdos relacionados de forma clara e discreta.',
    image: 'img/img-divider.png',
    link: `${baseUrl}/componentes/divider`,
    highlight: false,
  },
  {
    title: 'Form-Label',
    description: 'O Form-Label é um componente de suporte desenvolvido para rotular campos de formulário. Ele oferece recursos e comportamentos predefinidos que simplificam tanto a criação quanto o uso desses campos.',
    image: 'img/img-formlabel.png',
    link: `${baseUrl}/componentes/form-label`,
    highlight: true,
  },
  {
    title: 'Icon',
    description: 'É um componente de suporte que facilita a aplicação de iconografia em outros componentes, garantindo consistência visual e comportamental.',
    image: 'img/img-icon.png',
    link: `${baseUrl}/componentes/icon`,
    highlight: false,
  },
  {
    title: 'Message',
    description: 'É um componente de interface que fornece feedback ao usuário sobre o que ocorre no sistema. Ele exibe notificações, alertas e mensagens contextuais para informar de forma clara e direta o status de ações no sistema.',
    image: 'img/img-message.png',
    link: `${baseUrl}/componentes/message`,
    highlight: false,
  },
  {
    title: 'Text',
    description: 'Facilita a aplicação de formatações tipográficas em outros componentes, garantindo consistência visual e comportamental. É ideal para uso de títulos, subtítulos, rótulos ou palavras-chaves, otimizando a implementação tipográfica em componentes.',
    image: 'img/img-text.png',
    link: `${baseUrl}/componentes/text`,
    highlight: false,
  }
]

const cardsChannels = [
  {
    title: 'Repositório',
    description:
      'Acesse nosso repositório no GitLab e acompanhe de perto tudo o que está sendo desenvolvido pela nossa equipe.',
    image: 'img/gitlab-icon.png',
    imageDark: 'img/gitlab-icon-dark.png',
    link: 'https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core',
  },
  {
    title: 'Comunidade',
    description:
      'Tire dúvidas, colabore, ajude outros membros e participe de grupos de estudo em nossa comunidade no Discord.',
    image: 'img/discord-icon.png',
    imageDark: 'img/discord-icon-dark.png',
    link: 'https://discord.gg/NkaVZERAT7',
  },
  {
    title: 'Wiki',
    description:
      'Confira fluxos de trabalho, convenções e documentos úteis para aqueles que desejam se aprofundar no desenvolvimento do Design System.',
    image: 'img/hugo-icon.png',
    imageDark: 'img/hugo-icon-dark.png',
    link: 'https://gov.br/ds/wiki/',
  },
]

const dataIntro = [
  {
    title: 'Fundamentos',
    description:
      'Fundamentos visuais são os elementos básicos de design desenvolvidos para o Padrão Digital de Governo. Eles estabelecem uma linguagem visual consistente e coerente para todo design system. Os fundamentos visuais abordam temas importantes como cores, ilustrações, movimentos, tipografias, espaçamentos, sistemas de grids, ícones, entre outros.Todos esses elementos são cuidadosamente projetados de forma independente e global e se adequam ao estilo visual do design system. Além disso, suas diretrizes foram claramente estabelecidas, garantindo que todos tenham uma compreensão compartilhada de como cada fundamento deve ser aplicado. O objetivo desta abordagem é oferecer um design system robusto e consistente a fim de criar experiências únicas e coesas a todos os usuários.',
  },
  {
    title: 'Padrões',
    description: 'Padrões são conjuntos de práticas e modelos estabelecidos para guiar a consistência e a usabilidade dentro do design system. Encontre orientações e dicas para a criação de interfaces coerentes e intuitivas, abrangendo elementos como navegação, formulários, dropdown, uso de gráficos entre outros. Cada padrão é desenvolvido para funcionar tanto de forma isolada quanto integrada, assegurando alinhamento com a identidade visual e funcional do Design System. As diretrizes para esses padrões são claramente definidas, promovendo um entendimento comum de sua aplicação e garantindo que os layouts permaneçam eficazes e acessíveis.',
  },
  {
    title: 'Componentes',
    description: 'Os componentes são elementos de interface web oferecidos pelo Padrão Digital de Governo. São projetados usando tecnologia agnóstica com as linguagens HTML, CSS e JavaScript e encapsulam funcionalidades complexas em unidades independentes. Uma das principais características dos componentes é o poder de reutilização. São consistentes e fáceis de manter, pois, uma vez criados, eles podem ser facilmente reutilizados em diferentes partes de um sistema ou página web. Eles permitem que desenvolvedores construam sites e aplicativos mais rapidamente, sem a necessidade de recriar códigos ou conceitos de design já estabelecidos. Além disso, os componentes já trazem em sua estrutura padrões testados para garantir uma boa acessibilidade.',
  }
]

export { cardsChannels, cardsComponents, cardsFundamentals, cardsPattern, dataIntro }

