import { contentService } from "axiosConfig";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

type TabProperty = {
    name: string,
    docName: string
}



interface TabRendererIdentifier extends TabProperty {
    panel: string
}

interface useTabsProps {
    url: string,
    type: string,
    doc: string,
}


export const useTabs = ({ url, type, doc }: useTabsProps) => {
    const [tabs, setTabs] = useState<TabRendererIdentifier[]>([]);
    const [activeTab, setActiveTab] = useState<TabRendererIdentifier>()
    const { search, hash, state } = useLocation()
    const navigate = useNavigate()
    const activeTabIdentifier = search.split("?")[1]
    const tabFileName = [
        { name: 'Visão Geral', docName: 'visao-geral' },
        { name: 'Códigos e Utilitários', docName: 'codigos-utilitarios' },
        { name: 'Comportamentos', docName: 'comportamentos' },
        { name: 'Tokens', docName: 'tokens' },
        { name: 'Boas Práticas', docName: 'boas-praticas' },
        { name: 'Saiba Mais', docName: 'saiba-mais' },
        { name: 'Microcopy', docName: 'microcopy' },
        { name: 'Dicionário', docName: 'dicionario' }]
    const GetTabs = async () => {
        const { data: jsonDocs } = await contentService.get(`/docs/tabs.json`)
        const findedObject = jsonDocs[url].find((a: any) => Object.keys(a).includes(doc))
        const reducedData = tabFileName?.reduce((acc: TabRendererIdentifier[], currentItem: TabProperty, index: number) => {
            if (findedObject[doc]?.find((file: string) => file.includes(currentItem.docName))) {
                acc.push({ panel: `panel-${index + 1}`, name: currentItem.name, docName: currentItem.docName })
            }

            return acc

        }, [])
        setTabs(reducedData)
        if (!state) {
            const selectActiveTab = reducedData.find(data => data.docName === activeTabIdentifier) ?? reducedData[0]
            setActiveTab(selectActiveTab)
        }
        if (hash) {
            const splitedSearch = search.replace("?", "")
            const splitedHash = hash.replace("#", "")
            const findedActiveTab = reducedData.find(data => data.docName === splitedSearch) ?? reducedData[0]
            setActiveTab(findedActiveTab)
        }
    }

    useEffect(() => {
        GetTabs()
    }, [state])

    return {
        tabs,
        activeTab,
        setActiveTab
    }
}


