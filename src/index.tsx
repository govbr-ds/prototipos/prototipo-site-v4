import App from 'App'
import { InstantSearchWrapper } from 'components/InstantSearchWrapper/instantSearchWrapper'
import React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
const container = document.getElementById('root')
const root = createRoot(container!)

root.render(
  <React.StrictMode>
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <InstantSearchWrapper>
        <App />
      </InstantSearchWrapper>
    </BrowserRouter>
  </React.StrictMode>
)
