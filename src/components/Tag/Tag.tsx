import { memo, useEffect, useRef, useState } from "react";

const core = require("@govbr-ds/core/dist/core");

interface Props {
  title: string;
  color?: string;
}

export const Tag = memo((props: Props) => {
  const brTagRef = useRef<HTMLDivElement>(null);
  const [tagCore, setTagCore] = useState<any>();

  useEffect(() => {
    const coreTag = new core.BRTag("br-tag", brTagRef.current);
    setTagCore(coreTag);
  }, []);

  return (
    <span ref={brTagRef} className={`br-tag ${props.color} ml-3`}>
      <span>{props.title}</span>
    </span>
  );
});
