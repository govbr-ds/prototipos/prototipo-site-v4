import { cardsComponents, cardsFundamentals, cardsPattern } from '../../data/data.js';
import './cards.css';

const setHeading = (heading, title) =>
  heading === 'h2' ? <h2>{title}</h2> : <h3>{title}</h3>;


export default function CardList(props) {

  let dataTarget = null;
  let filteredCards = null;
  const titlePage = props.doc === 'highlights' ? (<h2>Destaques</h2>) : '';

  if (props.doc === 'highlights') {
    dataTarget = [...cardsFundamentals, ...cardsPattern, ...cardsComponents];
  } else if (props.doc === 'fundamentos') {
    dataTarget = cardsFundamentals;
  } else if (props.doc === 'padroes') {
    dataTarget = cardsPattern;
  } else if (props.doc === 'componentes') {
    dataTarget = cardsComponents;
  }

  if (dataTarget !== null) {
    const dataFiltered = props.dataFiltered || '';

    // Filtra primeiro
    let filteredData = dataTarget.filter(
      (item) =>
        startsWithOrderedLetters(item.title, dataFiltered) &&
        (props.doc === 'highlights' ? item.highlight === true : true)
    );

    // Depois limita o número de itens
    const maxItems = props.maxItems || filteredData.length;
    filteredData = filteredData.slice(0, maxItems);

    // Mapeia os resultados filtrados e limitados
    filteredCards = filteredData.map((item, index) => (
      <a key={index} href={item.link} className="card">
        <img src={item.image} alt={item.title} aria-hidden="true" />
        {setHeading(props.heading, item.title)}
        <p>{item.description}</p>
      </a>
    ));
  }

  function startsWithOrderedLetters(str1, str2) {
    const lowerStr1 = str1.toLowerCase()
    const lowerStr2 = str2.toLowerCase()

    let indexStr1 = 0
    for (let i = 0; i < lowerStr2.length; i++) {
      const char = lowerStr2[i]
      const charIndex = lowerStr1.indexOf(char, indexStr1)
      if (charIndex === -1 || charIndex !== i) {
        return false
      }
      indexStr1 = charIndex + 1
    }

    return true
  }

  return (
    <>
      {titlePage}
      <div className="panelCard">{filteredCards}</div>
    </>
  )
}
