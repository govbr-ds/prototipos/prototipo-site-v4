import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import './hero.css'

export default function Hero() {
  const baseUrl = process.env.PUBLIC_URL
  let title1 = 'Conheça a '
  let destaque = 'NOVA VERSÃO'
  let title2 = ' do Design System do Governo'
  let subtitle = 'Proporcionar uma experiência única e de qualidade aos cidadãos é um compromisso de todos.'
  let img = 'img/img-hero.png'
  const navigate = useNavigate()
  const [focusTitle, setFocusTitle] = useState(false)
  const [focusSubTitle, setFocusSubTitle] = useState(false)
  const [isKeyboardNav, setIsKeyboardNav] = useState(false);

  useEffect(() => {
    const handleKeyDown = () => setIsKeyboardNav(true);
    const handleMouseDown = () => setIsKeyboardNav(false);

    window.addEventListener('keydown', handleKeyDown);
    window.addEventListener('mousedown', handleMouseDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
      window.removeEventListener('mousedown', handleMouseDown);
    };
  }, []);

  return (
    <>
      <div className="hero">
        <div>
          <h1 onFocus={() => { setFocusTitle(true) }} onBlur={() => { setFocusTitle(false) }} id="main-content-title" className={`title ${focusTitle && isKeyboardNav ? 'focus' : ' '}`} tabIndex={0}>
            {title1}
            <span>{destaque}</span>
            {title2}
          </h1>
          <span onFocus={() => { setFocusSubTitle(true) }} onBlur={() => { setFocusSubTitle(false) }} className={`subtitle ${focusSubTitle && isKeyboardNav ? 'focus' : ' '}`} tabIndex={0}>{subtitle}</span>
          <div>
            <div className="br-magic-button large">
              <button
                className="br-button"
                type="button"
                onClick={() => {
                  navigate(`/introducao/como-comecar`)
                }}
              >
                <i className="fas fa-play" aria-hidden="true"></i>
                Como Começar
              </button>
            </div>
          </div>
        </div>
        <div>
          <img width="100%" src={img} alt="sample hero" />
        </div>
      </div>
    </>
  )
}
