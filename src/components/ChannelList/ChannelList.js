import { cardsChannels } from '../../data/data.js'
import './channels.css'

export default function ChannelList() {
  const channelList = cardsChannels.map((channel, index) => {
    return (
      <a key={index} className="channel" href={channel.link} target="_blank">
        <div>
          <img className='img-light-theme' width={index === 1 ? '65px' : '55px'} src={channel.image} alt={channel.title} />
          <img className='img-dark-theme' width={index === 1 ? '65px' : '55px'} src={channel.imageDark} alt={channel.title} />
        </div>
        <div>
          <h3>{channel.title}</h3>
          <p>{channel.description}</p>
        </div>
      </a>
    )
  })

  return (
    <>
      <h2>Nossos Canais</h2>
      <div className="channelPanel">{channelList}</div>
    </>
  )
}
