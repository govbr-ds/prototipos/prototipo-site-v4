import algoliasearch, { SearchClient } from "algoliasearch";
import { InstantSearch } from "react-instantsearch";

type InstantSearchProps = {
    children:React.ReactNode
}  


const  InstantSearchWrapper = ({children}:InstantSearchProps) => {
      const searchClient = algoliasearch('7T053RA1DW', '0b72c67626dc3e56c0b012ae9232e8f6')
  if (process.env.REACT_APP_INDICE_ALGOLIA) {
    const searchIndex = searchClient.initIndex(process.env.REACT_APP_INDICE_ALGOLIA)
  }

  const client:SearchClient = {
    ...searchClient,
    search(requests) {
      if (requests.every(({ params }) => !params?.query)) {
        return Promise.resolve({
          results: requests.map(() => ({
            hits: [],
            nbHits: 0,
            nbPages: 0,
            page: 0,
            processingTimeMS: 0,
            hitsPerPage: 0,
            exhaustiveNbHits: false,
            query: '',
            params: '',
          })),
        });
      }
      return searchClient.search(requests);
    }
  }

  return( 
  <InstantSearch indexName={process.env.REACT_APP_INDICE_ALGOLIA}  searchClient={client} future={{ preserveSharedStateOnUnmount: false }}>
    {children}
  </InstantSearch>
  )
}

export { InstantSearchWrapper };
