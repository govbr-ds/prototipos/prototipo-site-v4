import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';
import sendFeedback from 'utils/sendFeedback';
import './feedback.css';

const removeQuery = (url) => {
    return url.replace('?visao-geral', '').trim();
}

const Feedback = (props) => {
    const [clickLike, setClickLike] = useState(false)
    const [clickDislike, setClickDislike] = useState(false)
    const [checkResponse, setCheckResponse] = useState(false)
    const [sendResponse, setSendResponse] = useState(false)
    const [hasEvaluation, setHasEvaluation] = useState(false)
    const [questionResponse, setQuestionResponse] = useState('')
    const [loading, setLoading] = useState(false);

    let cookieUrls = Cookies.get('govbr-feedback-url') || [];
    let currentUrl = removeQuery(window.location.href)

    const activeLikeClass = clickLike ? 'active' : ''
    const activeDislikeClass = clickDislike ? 'active' : ''
    const disableLikeClass = clickDislike ? 'disabled' : ''
    const disableDislikeClass = clickLike ? 'disabled' : ''

    const showClass = clickLike || sendResponse || hasEvaluation ? 'show' : ''
    const showClassWarningOrSuccess = hasEvaluation ? 'warning' : 'success'
    const showDislikePanel = clickDislike ? 'show' : ''
    const showButton = checkResponse ? '' : 'disabled'
    const showPanel = sendResponse ? 'disabled' : ''

    useEffect(() => {
        setClickLike(false)
        setClickDislike(false)
        setCheckResponse(false)
        setSendResponse(false)
        setHasEvaluation(false)
        unCheckRadios()
    }, [props.tabId])

    function unCheckRadios() {
        const radioButtons = document.querySelectorAll('.dislike-panel input[type="radio"]');
        radioButtons.forEach(radioButton => {
            radioButton.checked = false;
        });
    }

    const handleClickLike = async () => {
        const checkUrl = cookieUrls.includes(currentUrl);

        if (!checkUrl) {
            if (!clickLike && !clickDislike) {

                setLoading(true);

                const jsonData = {
                    session: '',
                    like: 1,
                    comentario: '',
                    question: '',
                };

                try {
                    await sendFeedback(jsonData);

                } catch (error) {
                    console.error('Erro ao enviar feedback:', error);

                } finally {
                    let currentGroupUrls = []
                    currentGroupUrls.push(currentUrl)
                    const newCookieUrls = cookieUrls.concat(currentGroupUrls);
                    Cookies.set('govbr-feedback-url', newCookieUrls);
                    setClickLike(true);
                    setLoading(false);
                }
            }

        } else {
            setHasEvaluation(true)
        }
    }

    const handleClickDislike = () => {
        if (!clickDislike && !clickLike) {
            const checkUrl = cookieUrls.includes(currentUrl);
            if (!checkUrl) {
                setClickDislike(true)
                const targetComments = document.getElementById('text-comments');
                targetComments.value = '';
            } else {
                setHasEvaluation(true)
            }
        }
    }

    const setResponse = (resp) => {
        setCheckResponse(true)
        setQuestionResponse(resp)
    }

    const sendDislike = async () => {
        if (checkResponse) {

            setLoading(true);

            const targetComments = document.getElementById('text-comments');
            const comments = targetComments.value;

            const jsonData = {
                session: '',
                like: 0,
                comentario: comments,
                question: questionResponse,
            };

            try {
                await sendFeedback(jsonData);

            } catch (error) {
                console.error('Erro ao enviar feedback:', error);

            } finally {
                let currentGroupUrls = []
                currentGroupUrls.push(currentUrl)
                const newCookieUrls = cookieUrls.concat(currentGroupUrls);
                Cookies.set('govbr-feedback-url', newCookieUrls);
                setSendResponse(true)
                setLoading(false);
            }
        }
    }

    return (
        <div className="feedback-container">
            <label>Esta informação foi útil?</label>
            <div className='button-panel'>
                <button className={`br-button secondary circle large ${activeLikeClass} ${disableLikeClass}`} onClick={() => { handleClickLike() }} type="button" aria-label="Avalie Gostei"><i className="fas fa-thumbs-up" aria-hidden="true"></i></button>
                <button className={`br-button secondary circle large ${activeDislikeClass} ${disableDislikeClass}`} onClick={() => { handleClickDislike() }} type="button" aria-label="Avalie Não Gostei"><i className="fas fa-thumbs-down" aria-hidden="true"></i></button>
            </div>
            <div className={`dislike-panel ${showDislikePanel} ${showPanel}`}>

                <label>Gostaria de enviar mais detalhes sobre seu feedback?</label>
                <div className="br-radio">
                    <input id="op-1" type="radio" name="radio" value="op-1" onClick={() => { setResponse('1') }} />
                    <label htmlFor="op-1">Preciso de exemplos mais práticos</label>
                </div>
                <div className="br-radio">
                    <input id="op-2" type="radio" name="radio" value="op-2" onClick={() => { setResponse('2') }} />
                    <label htmlFor="op-2">Não encontrei a informação que eu desejava</label>
                </div>
                <div className="br-radio">
                    <input id="op-3" type="radio" name="radio" value="op-3" onClick={() => { setResponse('3') }} />
                    <label htmlFor="op-3">O código apresenta erro ou não funciona como eu esperava</label>
                </div>
                <div className="br-radio">
                    <input id="op-4" type="radio" name="radio" value="op-4" onClick={() => { setResponse('4') }} />
                    <label htmlFor="op-4">As informações ou exemplos não estão claros</label>
                </div>
                <div className='br-textarea small mt-3'>
                    <textarea id="text-comments" placeholder='Descreva mais detalhes (opcional)'></textarea>
                </div>
                <div className='text-right mt-3'>
                    <button className={`br-button primary ${showButton}`} type="button" aria-label="Enviar Avaliação" onClick={() => { sendDislike() }}>Enviar</button>
                </div>
            </div>
            {loading == true ? (<div className="loading medium"></div>) :
                (<span className={`feedback ${showClassWarningOrSuccess} ${showClass}`}>
                    {showClassWarningOrSuccess === 'warning' ? (
                        <>
                            <i className='fas fa-exclamation-triangle'></i>
                            Seu feedback já foi enviado!
                        </>
                    ) : (
                        <>
                            <i className='fas fa-check-circle'></i>
                            Obrigado pelo seu feedback!
                        </>
                    )}
                </span>)
            }
        </div>
    )
}

export default Feedback