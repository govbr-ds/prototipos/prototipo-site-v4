import { BRBreadcrumb } from '@govbr-ds/core/dist/core'
import { useEffect, useRef } from 'react'
import { capitalizeFirstLetter } from '../../utils/utils'
import './breadcrumb.css'


let coreBreadcrumb = null

const Breadcrumb = (props) => {
  const baseUrl = process.env.PUBLIC_URL
  const brBreadcrumbRef = useRef()

  useEffect(() => {
    if (!coreBreadcrumb) {
      coreBreadcrumb = new BRBreadcrumb('br-breadcrumb', brBreadcrumbRef.current)
    }
  }, [])

  const dados = props.data
  const breadcrumbList = dados.map((item, index) => {
    return (
      <li key={`crumb-id-${index}`} className="crumb" data-active>
        <i className="icon fas fa-chevron-right"></i>
        {item.url === undefined ? (
          <label>{capitalizeFirstLetter(item.label)}</label>
        ) : (
          <a href={item.url}>{capitalizeFirstLetter(item.label)}</a>
        )}
      </li>
    )
  })

  return (
    <div className="br-breadcrumb" ref={brBreadcrumbRef}>
      <ul className="crumb-list">
        <li className="crumb home">
          <a className="br-button circle" href={`/home`}>
            <span className="sr-only">Página Inicial</span>
            <i className="fas fa-home"></i>
          </a>
        </li>
        {breadcrumbList}
      </ul>
    </div>
  )
}

export default Breadcrumb
