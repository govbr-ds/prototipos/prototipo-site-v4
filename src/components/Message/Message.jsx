import Cookies from 'js-cookie';
import './message.css';

const Message = ({ message, setShowMessage }) => {

    const setCookie = (name, value, days) => {
        Cookies.set(name, value.toString(), { expires: days });
        setShowMessage(value)
    };

    return (
        <div className={`message ${message ? 'show-message' : ''}`}>
            <div className='container-lg'>
                <div className='text-message'>
                    <i className="fas fa-info-circle" aria-hidden="true"></i>
                    <span>
                        <strong>Você está acessando a versão 4 do Padrão Digital de Governo.</strong> Esta versão está em desenvolvimento, com atualizações contínuas para que você possa testar as novidades e nos enviar <i>feedback</i>. Para novas implementações ou soluções, recomendamos o uso da <a href="https://www.gov.br/ds/home" target='_blank'>versão 3</a>.
                    </span>
                </div>
                <div>
                    <button className="br-button circle large" onClick={() => { setCookie("govbr-status-message", false, 7) }} type="button" aria-label="Fechar mensagem"><i className="fas fa-times" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    )
}

export default Message