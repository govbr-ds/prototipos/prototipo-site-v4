import { contentService } from 'axiosConfig';
import Markdown from 'markdown-to-jsx';
import { useEffect, useState } from 'react';
import { formatMarkdown } from '../../utils/formatMarkDown';
interface MarkdownRendererProps {
  path:string,
  file:string
}

const MarkdownRenderer = ({path,file}:MarkdownRendererProps) => {
  const [markdownContent, setMarkdownContent] = useState('')
  const [isLoading, setIsLoading] = useState(true)
  const baseUrl = process.env.PUBLIC_URL
    let url = `${baseUrl}/${path}${file}`

  
    const GetContent = async () => {
    const {data} = await contentService.get(`/${path}${file}`)
    const {content,metadata} = formatMarkdown(data,baseUrl,path)
    setMarkdownContent(content)
    setIsLoading(false)
  }

  useEffect(() => {
   GetContent()
  }, [])

  if (isLoading) {
    return <div className="loading medium"></div>
  }

  return <Markdown>{markdownContent}</Markdown>

}

export default MarkdownRenderer
