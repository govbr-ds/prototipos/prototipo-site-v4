import Cookies from 'js-cookie';
import { useEffect, useRef, useState } from 'react';
import './select-theme.css';

let coreSelect = null


let valorCookie = Cookies.get('govbr-theme');

if (valorCookie === undefined) {
    Cookies.set('govbr-theme', 'theme-light')
    valorCookie = Cookies.get('govbr-theme')
}


export const SelectTheme = () => {
    const baseUrl = process.env.PUBLIC_URL
    const [label, setLabel] = useState()
    const [showSelect, setShowSelect] = useState(false)
    const [count, setCounter] = useState(0)
    const brSelectRef = useRef()
    const selectRef = useRef()
    function setTheme(theme) {
        let el = document.getElementById("stylePrism")

        if (theme === 'Dark') {
            // tema dark estilo geral
            var linkElement = document.createElement("link");
            linkElement.className = 'style-dark'
            linkElement.setAttribute("rel", "stylesheet");
            linkElement.setAttribute("href", `${baseUrl}/styles/dark-theme.css`);
            var headElement = document.head || document.getElementsByTagName("head")[0];
            headElement.appendChild(linkElement);

            // tema dark estilo pre code
            el.setAttribute("href", "https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.0/themes/prism-okaidia.min.css");

        } else {
            el.setAttribute("href", "https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.0/themes/prism.min.css");

            var styleDarkElments = document.getElementsByClassName('style-dark');
            if (styleDarkElments) {
                var styleDarkElmentsArray = Array.from(styleDarkElments);
                styleDarkElmentsArray.forEach(function (elementStyleDark) {
                    elementStyleDark.parentNode.removeChild(elementStyleDark);
                });
            }
        }
    }

    const handleThemeChange = (e) => {
        if (e.target.checked) {
            const selectedTheme = e.target.value;
            Cookies.set('govbr-theme', selectedTheme);
            selectedTheme === 'theme-dark' ? setTheme('Dark') : setTheme('Light')
            selectedTheme === 'theme-dark' ? setLabel('Dark') : setLabel('Light')
        }
        setShowSelect(false)
    };

    useEffect(() => {
        valorCookie === 'theme-dark' ? setTheme('Dark') : setTheme('Light')
        valorCookie === 'theme-dark' ? setLabel('Dark') : setLabel('Light')
    }, []);

    const handleClickOutside = (event) => {
        if (brSelectRef.current && !brSelectRef.current.contains(event.target)) {
            setShowSelect(false);
        }
    };

    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    const showList = () => {
        setShowSelect((preState) => !preState)
    }

    const keyDownEvent = (e) => {
        if (!showSelect && e.keyCode === 13) {
            showList()
            return;
        }
        if (showSelect && count === 0 && e.keyCode === 40) {
            setCounter(1)
            e.preventDefault()
            selectRef.current.children[0].focus()
            return
        }
        if (showSelect && count === 1 && e.keyCode === 40) {
            e.preventDefault()
            setCounter(0)
            selectRef.current.children[1].focus()
            return
        }
        if (showSelect && count === 0 && e.keyCode === 38) {
            setCounter(1)
            e.preventDefault()
            selectRef.current.children[0].focus()

            return
        }
        if (showSelect && count === 0 && e.keyCode === 13) {
            e.preventDefault()
            Cookies.set('govbr-theme', 'theme-dark');
            setTheme("Dark")
            setLabel("Dark")
            setShowSelect(false)
            return
        }
        if (showSelect && count === 1 && e.keyCode === 13) {
            e.preventDefault()
            Cookies.set('govbr-theme', 'theme-light');
            setTheme("Light")
            setLabel("Light")
            setShowSelect(false)
            return
        }
        if (e.keyCode === 27 || e.keyCode == 9) {
            setShowSelect(false)
        }

    }



    return (
        <div
            id="selectBr"
            className="br-select custom-v4"
            data-tip
            data-tooltip-id="botao-tooltip-8"
            ref={brSelectRef}
        >
            <div onClick={showList} className="br-input" onKeyDown={(e) => keyDownEvent(e)} aria-label="Escolher tema">
                <input id="select-simple" type="text" placeholder={label} readOnly />
                <button className="br-button" type="button" aria-label="Exibir lista" tabIndex="-1" data-trigger="data-trigger">
                    <i className={`${showSelect ? "fas fa-angle-up" : "fas fa-angle-down"}`} aria-hidden="true"></i>
                </button>
            </div>
            {
                <div className="br-list expanded" tabIndex="0" onKeyDown={(e) => keyDownEvent(e)} ref={selectRef} expanded={showSelect ? "true" : undefined} >
                    <div className={`br-item theme-light ${label === 'Light' && "selected"}`} tabIndex="-1" >
                        <div className="br-radio">
                            <input id="theme-light" type="radio" name="estados-simples" value="theme-light" onChange={handleThemeChange} />
                            <label htmlFor="theme-light">Light</label>
                        </div>
                    </div>
                    <div className={`br-item theme-dark ${label === 'Dark' && "selected"}`} tabIndex="-1">
                        <div className="br-radio">
                            <input id="theme-dark" type="radio" name="estados-simples" value="theme-dark" onChange={handleThemeChange} />
                            <label htmlFor="theme-dark">Dark</label>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}