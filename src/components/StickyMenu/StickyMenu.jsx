import { useEffect } from 'react'
import './StickyMenu.css'

function removeActiveClass() {
  const itens = document.querySelectorAll('.br-item')
  itens.forEach(function (item) {
    item.classList.remove('item-active')
  })
}

let hasClickTarget = 0

const StickyMenu = (props) => {
  const list = props.selectedList

  const targetAnchor = (anchor, item, hasTab) => {

    const itemEl = document.getElementById(item)

    if (itemEl.id === 'anchor-item-0' && hasTab == true) {
      window.scrollTo({
        top: 690,
        behavior: 'smooth',
      })
    } else {
      const minhaDiv = document.getElementById(props.tabId)
      const myAnchors = Array.from(minhaDiv.getElementsByTagName('h2'))

      const targetAnchor = myAnchors.find((item) => item.textContent.toLowerCase() === anchor.toLowerCase())

      if (targetAnchor) {
        function setPosicionTop() {
          let hasStickyClass = document.getElementsByClassName('sticky')
          let addValue = 0

          if (hasStickyClass.length > 0 && hasClickTarget === 0) {
            addValue = 56
            hasClickTarget++
          }

          return addValue
        }

        let targetPosition = targetAnchor.offsetTop - 100

        if (hasTab == true) {
          targetPosition = targetAnchor.offsetTop - (160 + setPosicionTop())
        }

        // Função para rolar suavemente até a posição alvo
        const scrollToTarget = (start, end) => {
          let currentPosition = start || window.pageYOffset
          const distance = Math.abs(end - currentPosition)

          // Define a direção da rolagem
          const direction = currentPosition < end ? 1 : -1

          const duration = 800 // Duração da animação em milissegundos
          const step = 20 // Intervalo de tempo entre as etapas

          const scrollStep = distance / (duration / step)

          const scrollInterval = setInterval(() => {
            currentPosition += scrollStep * direction

            if ((direction === 1 && currentPosition >= end) || (direction === -1 && currentPosition <= end)) {
              clearInterval(scrollInterval)
              currentPosition = end
            }

            window.scrollTo(0, currentPosition)

            if (currentPosition === end) {
              clearInterval(scrollInterval)
            }
          }, step)
        }

        scrollToTarget(window.pageYOffset, targetPosition)
      }
    }
  }

  useEffect(() => {
    const handleScroll = () => {
      let pageTab = document.getElementById(props.tabId)
      const scrollY = window.scrollY

      let h2Elements = null

      if (pageTab) {
        h2Elements = Array.from(pageTab.getElementsByTagName('h2'))
      }

      let targetEl = document.getElementById('contextualMenu')
      let targetFinal = null

      if (targetEl) {
        targetFinal = targetEl.getElementsByClassName('br-item')
      }

      if (targetFinal && targetFinal.length > 0) {
        var pageHeight = document.documentElement.scrollHeight
        var windowHeight = window.innerHeight
        var scrollPosition =
          window.scrollY || window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop

        if (h2Elements && targetEl) {
          let value = 0
          if (props.hasTab == true) {
            value = 1
          }
          for (let i = 0; i < h2Elements.length; i++) {
            let h2Element = h2Elements[i]
            let rect = h2Element.getBoundingClientRect()
            let el = document.getElementById("contextualMenu")
            let footerHeight = 105;

            if (scrollY <= 100) {
              removeActiveClass()
              targetFinal[0].classList.add('item-active')
              hasClickTarget = 0
            }
            if (rect.top > 100 && rect.top <= 400) {
              removeActiveClass()
              if (targetFinal[i + value]) {
                targetFinal[i + value].classList.add('item-active')
              }
              break
            }
            if ((scrollPosition + windowHeight) + footerHeight >= pageHeight) {
              removeActiveClass()
              el.style.height = windowHeight - footerHeight - 150 + 'px';

              var lastIndex = targetFinal.length - 1
              if (lastIndex >= 0) {
                targetFinal[lastIndex].classList.add('item-active')
              }
            } else {
              el.style.height = '85vh';
            }
          }
        }
      }
    }
    window.addEventListener('scroll', handleScroll)
  }, [][list])


  return (
    <div className="br-list" role="list">
      {list === null ? (
        <div className="containerLoading">
          <div className="loading medium"></div>
        </div>
      ) : (
        <div>
          {list.length > 0 && (
            <div className="header">
              <div className="title">CONTEÚDO</div>
            </div>
          )}
          {list.map((item, index) => (
            <div id={`anchor-item-${index}`} key={index} className="br-item" role="listitem">
              <a
                href="#"
                onClick={(event) => {
                  event.preventDefault()
                  targetAnchor(item, `anchor-item-${index}`, props.hasTab)
                }}
              >
                {item}
              </a>
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

export default StickyMenu
