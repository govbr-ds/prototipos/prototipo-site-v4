import { useEffect, useRef, useState } from 'react'
import { SearchBox, useInstantSearch } from 'react-instantsearch'


import { useLocation } from 'react-router-dom'
import './search.css'



const customTranslations = {
  placeholder: 'Digite uma palavra-chave',
}

const Search = (props) => {
  const [query, setQuery] = useState('')
  const [isShowSearchResult, setShowSearchResult] = useState(false)
  const hitsRef = useRef(null)
  const focus = useRef(null)
  const { hash } = useLocation()
  let timeoutId = null





  const handleKeyPress = (e) => {
    if (e.keyCode === 27 && props.showSearch) {
      props.setShow(false)
    }
    if (e.keyCode === 40 && props.showSearch && hitsRef.current) {
      e.preventDefault()
      const focusElement = document.activeElement
      const list = [...hitsRef.current.children]
      if (list.every(element => focusElement !== element.children[0])) {
        list[0].children[0].focus()
      }
      else {
        focusElement.parentElement?.nextElementSibling?.lastElementChild?.focus()
      }
    }
    if (e.keyCode === 38 && props.showSearch && hitsRef.current) {
      e.preventDefault()
      const focusElement = document.activeElement
      const list = [...hitsRef.current.children]
      const isFirstElement = list[0].children[0] === focusElement
      if (isFirstElement) {
        const inputFocus = document.querySelector(".ais-SearchBox-input")
        inputFocus?.focus()
      }
      else {
        focusElement.parentElement?.previousElementSibling?.lastElementChild.focus()
      }

    }
  }

  useEffect(() => {
    document.addEventListener("keydown", handleKeyPress)
    return () => {
      document.removeEventListener("keydown", handleKeyPress)
    }
  }, [])

  useEffect(() => {
    if (focus.current && hash === "#main-searchbox") {
      setTimeout(() => {
        focus.current.firstElementChild.focus()
      }, 50)
    }
  }, [hash, props.showSearch])





  const queryHook = (query, search) => {
    if (timeoutId) {
      clearTimeout(timeoutId)
    }
    timeoutId = setTimeout(() => {
      search(query)
    }, 500)
  }

  return (
    <>
      <SearchBox queryHook={queryHook} translations={customTranslations} autoFocus={true} formRef={focus} />
      <HitCustom hitsRef={hitsRef} handleKeyPress={handleKeyPress} setShowSearchResult={setShowSearchResult} />
    </>
  )
}


const HitCustom = ({ handleKeyPress, hitsRef, setShowSearchResult }) => {
  const { results: { hits: items } } = useInstantSearch()

  const handleSearchAriaLabel = () => {
    const inputElement = document.querySelector(".ais-SearchBox-input")
    if (items.length > 0) {
      inputElement.setAttribute("aria-label", "Campo de busca, lista de resultados expandida")
    }
    else {
      inputElement.setAttribute("aria-label", "Campo de busca, lista de resultados recolhida")
    }
  }

  useEffect(() => {
    handleSearchAriaLabel()
  }, [items])

  if (items.length > 0) {
    return (
      <div className='ais-Hits'>
        <ul role='listbox' aria-label={`resultado da busca ${items.length} items`} ref={hitsRef} className='ais-Hits-list'>
          {
            items.map(item => (
              <li key={item.link} className='ais-Hits-item'>
                <a role='option' href={`${process.env.PUBLIC_URL}${item.link}`}>
                  <span>{item.categoria}</span>
                  <span className="tag-abas"> | {item.tab}</span>
                  <p>{item.title}</p>
                </a>
              </li>
            ))
          }
        </ul>
      </div>
    )

  }
  return null

}




export default Search
