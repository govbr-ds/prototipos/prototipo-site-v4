import { BRCarousel } from '@govbr-ds/core/dist/core'
import { useEffect, useRef } from 'react'
import { dataCarousel } from '../../data/dataCarousel'
import './carousel.css'

let coreCarousel = null

const Carousel = () => {
  const brCarouselRef = useRef()

  useEffect(() => {
    if (!coreCarousel) {
      coreCarousel = new BRCarousel('br-carousel', brCarouselRef.current)
    }

    const handleKeyDown = (event) => {
      if (event.altKey && (event.key === 'ArrowRight' || event.key === 'ArrowLeft')) {
        // Previne o comportamento padrão do NVDA e outros navegadores
        event.preventDefault()

        if (event.key === 'ArrowRight') {
          document.getElementById('bt-right').click()
        } else if (event.key === 'ArrowLeft') {
          document.getElementById('bt-left').click()
        }

        setTimeout(() => {
          let element = document.querySelector('[active]')
          document.querySelectorAll('.focus').forEach((el) => {
            el.classList.remove('focus')
          })

          if (element) {
            let link = element.querySelector('a')
            if (link) {
              link.focus()
            } else {
              let div = element.querySelector('.divFocus');
              div.classList.add('focus')
              div.focus()
            }
          }
        }, 1000)
      }
    }

    document.addEventListener('keydown', handleKeyDown)
    return () => {
      document.removeEventListener('keydown', handleKeyDown)
    }
  }, [])

  function checkImage(path, title) {
    if (path !== '') {
      return <img src={path} alt={`Imagem ${title}`} />
    } else {
      return ''
    }
  }

  return (
    <div className="br-carousel" data-circular="true" ref={brCarouselRef}>
      <div className="carousel-button">
        <button id="bt-left" tabIndex="-1" className="br-button carousel-btn-prev terciary circle" type="button" aria-hidden="true">
          <i className="fas fa-chevron-left" aria-hidden="true"></i>
        </button>
      </div>
      <div className="carousel-stage">
        {dataCarousel.map((item, index) => {
          return (
            <div key={item.id} className="carousel-page" active={index === 0 ? 'active' : undefined}>
              <div className={`carousel-content ${item.bg}`}>
                {item.link !== '' ? (
                  <a tabIndex="0" href={item.link} target="_blank" aria-label={`${index === 0 ? 'Carrossel - Use Alt + Seta Esquerda ou Alt + Seta Direita para navegar pelos itens do carrossel.' : ''} ${item.title}. ${item.description}`}>
                    <div>
                      <h3>{item.title}</h3>
                      <p>{item.description}</p>
                    </div>
                    <div className="center">{checkImage(item.image, item.title)}</div>
                  </a>
                ) : (
                  <div tabIndex="0" className='divFocus' aria-label={`${index === 0 ? 'Carrossel - Use Alt + Seta Esquerda ou Alt + Seta Direita para navegar pelos itens do carrossel.' : ''} ${item.title}. ${item.description}`}>
                    <div>
                      <h3>{item.title}</h3>
                      <p>{item.description}</p>
                    </div>
                    <div className="center">{checkImage(item.image, item.title)}</div>
                  </div>
                )}
              </div>
            </div>
          )
        })}
      </div>
      <div className="carousel-button">
        <button id="bt-right" tabIndex="-1" className="br-button carousel-btn-next terciary circle" type="button" aria-hidden="true">
          <i className="fas fa-chevron-right" aria-hidden="true"></i>
        </button>
      </div>
      <div className="carousel-step">
        <div className="br-step" data-initial="1" data-type="simple">
          <div className="step-progress">
            {dataCarousel.map((item) => {
              return (
                <button tabIndex="-1" key={item.id} className="step-progress-btn" type="button" aria-hidden="true">
                  <span className="step-info">Exemplo de Rótulo</span>
                </button>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Carousel
