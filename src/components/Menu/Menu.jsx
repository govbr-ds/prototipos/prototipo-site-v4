import { BRMenu } from '@govbr-ds/core/dist/core'
import MenuFooterItem from 'components/Menu/MenuFooterItem'
import MenuItem from 'components/Menu/MenuItem'
import { memo, useEffect, useRef, useState } from 'react'
import { useLocation } from 'react-router-dom'
import MenuLogoDark from "../../assets/img/logo-govbr-dark.png"
import MenuLogo from "../../assets/img/logo-govbr.png"
import './menu.css'


let coreMenu = null

export const Menu = memo(() => {
  const baseUrl = process.env.PUBLIC_URL
  const brMenuRef = useRef()
  const [showMenu, setShowMenu] = useState(false)
  const { pathname } = useLocation()

  useEffect(() => {
    if (!coreMenu) {
      coreMenu = new BRMenu('br-menu', brMenuRef.current)
    }

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [])


  const handleKeyDown = (event) => {
    if (event && event.keyCode === 27) {
      ResetMenu()
      setShowMenu(false)
    }

    if (event.altKey) {
      switch (event.code) {
        case 'Digit1':
        case 'Digit3':
        case 'Digit4':
          setShowMenu(false);
          break;
        default:
          break;
      }
    }

    // Verificar se o foco está em um item do SkipLink
    const skipLinkItems = document.querySelectorAll('.br-skiplink a');
    const activeElement = document.activeElement;
    const isSkipLinkFocused = Array.from(skipLinkItems).includes(activeElement);

    if (event.key === 'Enter' && isSkipLinkFocused) {
      setShowMenu(false);
    }
  };

  const ResetMenu = () => {
    if (pathname === "/home" || pathname === "") {
      const hiddenElements = document.querySelectorAll(".menu-item")
      const activeElement = document.querySelector(".side-menu.active")
      if (activeElement) {
        activeElement.classList.remove("active")
      }
      hiddenElements.forEach(element => {
        element.removeAttribute("hidden")
      })
    }
  }

  useEffect(() => {
    ResetMenu()
  }, [pathname])

  return (
    <div tabIndex={0} id="main-navigation" onKeyDown={(e) => handleKeyDown(e)} onClick={() => setShowMenu(true)} className={`br-menu ${showMenu ? 'active' : ''}`} ref={brMenuRef} role="navigation">
      <h2 id="main-navigation-title" tabIndex="0" onFocus={(e) => setShowMenu(true)} className='title-menu-hidden'>Menu Principal</h2>
      <div className="menu-container position-static shadow-lg-right">
        <div className="container-lg">
          <div className="menu-panel h-auto position-static">
            <div className="menu-header">
              <div className="menu-title">
                <img className='img-light-theme' width="50px" src={MenuLogo} alt="logo govbr" />
                <img className='img-dark-theme' width="50px" src={MenuLogoDark} alt="logo govbr" />
              </div>
              <div className="menu-close">
                <button className="br-button circle" type="button" aria-label="Fechar o menu" onClick={ResetMenu} data-dismiss="menu">
                  <i className="fas fa-times" aria-hidden="true"></i>
                </button>
              </div>
            </div>
            <nav className="menu-body" aria-label="Menu principal">
              <MenuItem />
            </nav>
            <div className="menu-footer">
              <MenuFooterItem />
              <div className="menu-info">
                <div className="text-center text-down-01">
                  O Padrão Digital de Governo utiliza as licenças CC0 1.0 Universal e MIT.
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="menu-scrim" data-dismiss="menu" ></div>
      </div>
    </div>
  )
})
