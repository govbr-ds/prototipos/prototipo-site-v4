import React from 'react'
import { itemsFooterMenu } from '../../data/dataMenu'
import './menu.css'

export default function MenuFooterItem() {
  function getDivider(divider) {
    if (divider) {
      return <span className="br-divider mt-4"></span>
    } else {
      return ''
    }
  }

  const result = itemsFooterMenu.map((item, index) => {
    return (
      <React.Fragment key={index}>
        <a className="flex justify-between" href={item.link} target="_blank">
          <span className="mr-1">{item.label}</span>
          <i className="fas fa-external-link-alt" aria-hidden="true"></i>
        </a>
        {getDivider(item.divider)}
      </React.Fragment>
    )
  })

  return <div className="menu-links p-2">{result}</div>
}
