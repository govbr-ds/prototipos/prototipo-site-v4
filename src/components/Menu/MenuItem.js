import React, { useState } from 'react'
import { useLocation } from 'react-router-dom'
import { itemsMenu } from '../../data/dataMenu'
import './menu.css'

export default function MenuItem(props) {
  const [active, setActive] = useState("")
  const { pathname } = useLocation()
  function getHightlight(hightlight) {
    return hightlight ? 'hightlight' : ''
  }



  const ActiveMenuByKeyBoard = (targetElement, hiddenElements) => {
    targetElement.classList.add("active")
    hiddenElements.forEach(element => {
      if (!targetElement.contains(element)) {
        element.setAttribute("hidden", "")
      }
    })
  }

  const HideMenuByKeyBoard = (targetElement, hiddenElements) => {
    targetElement.classList.remove("active")
    hiddenElements.forEach(element => {
      element.removeAttribute("hidden")
    })
  }

  const MenuOnKeyDonw = (e) => {
    if (e.keyCode === 13) {
      const hiddenElements = document.querySelectorAll(".menu-item")
      const targetElementClassList = e.target.parentElement.classList
      if (e.target.hasAttribute('href')) {
        return;
      }
      if (!targetElementClassList.contains("active")) {
        ActiveMenuByKeyBoard(e.target.parentElement, hiddenElements)
      }
      else {
        HideMenuByKeyBoard(e.target.parentElement, hiddenElements)
      }
    }
  }

  function checkLink(link, label, icon) {
    if (link === undefined || link === '') {
      return (
        <div className="menu-item">
          {getIcon(icon)}
          <span className="content">{label}</span>
        </div>
      )
    } else {
      return (
        <div className="menu-item link">
          <a href={link}>
            {getIcon(icon)}
            <span className="content">{label}</span>
          </a>
        </div>
      )
    }
  }

  function getIcon(icon) {
    if (icon !== undefined || icon !== '' || icon !== null) {
      return (
        <span className="icon">
          <i className={icon} aria-hidden="true"></i>
        </span>
      )
    } else {
      return ''
    }
  }

  function getChildren(filhos) {
    const selectedOption = filhos?.find(element => pathname === element.link)
    if (selectedOption) {
      const targetElement = document.querySelector(`[href="${selectedOption.link}"]`)
      const hiddenElements = document.querySelectorAll(".menu-item")
      const targetElementClassList = targetElement?.parentElement?.parentElement?.parentElement.classList
      if (targetElementClassList && !targetElementClassList?.contains("active")) {
        ActiveMenuByKeyBoard(targetElement?.parentElement?.parentElement?.parentElement, hiddenElements)
        targetElement?.classList?.add("selectedOptionMenu")
      }

    }

    if (filhos === undefined || filhos.length === '') {
      return ''
    } else {
      return (
        <ul>
          {filhos.map((item, index) => {
            return (
              <li key={index} onKeyDown={(e) => MenuOnKeyDonw(e)} className={`${item.disabled ? 'disabled' : ''}`}>
                <a tabIndex={0} className={`menu-item ${getHightlight(item.hightlight)}`} href={item.link}>
                  <span className="content">{item.label}</span>
                </a>
                {getChildren(item.filhos)}
              </li>
            )
          })}
        </ul>
      )
    }
  }

  const result = itemsMenu.map((item, index) => {
    return (
      <React.Fragment key={index}>
        {checkLink(item.link, item.label, item.icon)}
        {getChildren(item.filhos)}
      </React.Fragment>
    )
  })

  return <div className="menu-folder">{result}</div>
}
