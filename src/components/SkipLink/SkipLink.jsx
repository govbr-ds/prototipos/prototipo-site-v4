import { useEffect } from 'react';

const SkipLink = () => {
    useEffect(() => {
        const handleKeyDown = (event) => {
            if (event.altKey) {
                switch (event.code) {
                    case 'Digit1':
                        document.querySelector('a[href="#main-content-title"]').click();
                        break;
                    case 'Digit2':
                        document.querySelector('a[href="#main-navigation-title"]').click();
                        break;
                    case 'Digit3':
                        document.querySelector('a[href="#main-searchbox"]').click();
                        break;
                    case 'Digit4':
                        document.querySelector('a[href="#footer-title"]').click();
                        break;
                    default:
                        console.log('No matching key');
                        break;
                }
            }
        };

        window.addEventListener('keydown', handleKeyDown);

        return () => {
            window.removeEventListener('keydown', handleKeyDown);
        };
    }, []);

    return (

        <nav className="br-skiplink full" role="menubar" tabIndex={0}>
            <a className="br-item" href="#main-content-title" role="menuitem" accessKey="1" tabIndex={0}>Ir para o conteúdo <span aria-hidden="true" className="br-tag text ml-1">1</span></a>
            <a className="br-item" href="#main-navigation-title" role="menuitem" accessKey="2" tabIndex={0}>Ir para o menu <span aria-hidden="true" className="br-tag text ml-1">2</span></a>
            <a className="br-item" href="#main-searchbox" role="menuitem" accessKey="3" tabIndex={0}>Ir para a busca <span aria-hidden="true" className="br-tag text ml-1">3</span></a>
            <a className="br-item" href="#footer-title" role="menuitem" accessKey="4" tabIndex={0}>Ir para o rodapé <span aria-hidden="true" className="br-tag text ml-1">4</span></a>
        </nav>
    );
};

export default SkipLink;
