import Message from 'components/Message/Message'
import Search from 'components/Search/Search'
import { SelectTheme } from 'components/SelectTheme/SelectTheme'
import { useEffect, useState } from 'react'
import { Tooltip } from 'react-tooltip'

import { useNavigate } from 'react-router-dom'
import './header.css'




export const HeaderCompact = ({ setShowMessage, message }) => {
  const baseUrl = process.env.PUBLIC_URL
  const navigate = useNavigate()
  const [title, setTitle] = useState('Padrão Digital de Governo')
  const [tagtext, setTagtext] = useState('Versão 4 Alpha')
  const [isSticky, setIsSticky] = useState(false)
  const [showSearchTab, setShowSearchTab] = useState(false)
  const [showChanels, setShowChanels] = useState(false)

  function verificarLarguraDaTela() {
    const width = window.innerWidth;
    let title = '';
    let tagtext = '';

    if (width <= 620) {
      title = 'GOVBR-DS';
      tagtext = 'V4';
    } else if (width <= 820) {
      title = 'Padrão Digital GovBR';
      tagtext = 'V4';
    } else {
      title = 'Padrão Digital de Governo';
      tagtext = 'Versão 4 Alpha';
    }

    setTitle(title);
    setTagtext(tagtext);
  }

  useEffect(() => {

    const handleScroll = () => {
      const menu = document.querySelector('.header-top')

      if (menu) {
        const initialMenuOffset = menu.offsetTop
        setIsSticky(window.pageYOffset > initialMenuOffset)
      }
    }

    verificarLarguraDaTela()
    window.addEventListener('scroll', handleScroll)
    window.addEventListener('resize', verificarLarguraDaTela)

    return () => {
      window.removeEventListener('scroll', handleScroll)
      window.removeEventListener('resize', verificarLarguraDaTela);
    }

  }, [])

  return (
    <div className='container-header-fixed'>
      <Message message={message} setShowMessage={setShowMessage} />
      <header id="Header" className={`br-header compact ${isSticky ? ' sticky' : ''}`}>
        <div className="container-lg">
          <div className="header-top">
            <div className="header-logo">
              <img src="img/logo-govbr-dark.png" alt="logo" />
            </div>
            <div className="header-actions">
              <SelectTheme />
              <Tooltip id="botao-tooltip-8" place="left" effect="solid" className="tooltip-ds">
                <span>Tema</span>
              </Tooltip>
              <span className="br-divider vertical mx-half mx-sm-1"></span>
              <div className={`header-functions dropdown ${showChanels ? 'show' : ""}`}>
                <button
                  data-tip
                  data-tooltip-id="botao-tooltip-6"
                  className="br-button circle large"
                  type="button"
                  data-toggle="dropdown"
                  aria-label="Nossos Canais"
                  onClick={() => setShowChanels(prevState => !prevState)}
                >
                  <i className="fas fa-th" aria-hidden="true"></i>
                </button>

                <div className="br-list">
                  <div className="header">
                    <div className="title">Nossos Canais</div>
                  </div>
                  <div className="br-item">
                    <button
                      data-tip
                      data-tooltip-id="botao-tooltip-1"
                      className="br-button circle large"
                      type="button"
                      aria-label="Repositório - Gitlab"
                      onClick={() => window.open('https://gitlab.com/govbr-ds/ds/dev/govbr-ds-dev-core', '_blank')}
                    >
                      <i className="fab fa-gitlab" aria-hidden="true"></i>
                      <span className="text">Repositório - Gitlab</span>
                    </button>
                    <Tooltip id="botao-tooltip-1" place="bottom" effect="solid" className="tooltip-ds">
                      <span>Repositório</span>
                    </Tooltip>
                  </div>
                  <div className="br-item">
                    <button
                      data-tip
                      data-tooltip-id="botao-tooltip-2"
                      className="br-button circle large"
                      type="button"
                      aria-label="Comunidade - Discord"
                      onClick={() => window.open('https://discord.com/invite/NkaVZERAT7', '_blank')}
                    >
                      <i className="fab fa-discord" aria-hidden="true"></i>
                      <span className="text">Comunidade - Discord</span>
                    </button>
                    <Tooltip id="botao-tooltip-2" place="bottom" effect="solid" className="tooltip-ds">
                      <span>Comunidade</span>
                    </Tooltip>
                  </div>
                  <div className="br-item">
                    <button
                      data-tip
                      data-tooltip-id="botao-tooltip-3"
                      className="br-button circle large bt-img-hugo"
                      type="button"
                      aria-label="Wiki"
                      onClick={() => window.open('https://govbr-ds.gitlab.io/tools/govbr-ds-wiki/', '_blank')}
                    >
                      <img className='img-light-theme' width="18px" aria-hidden="true" src={`${baseUrl}/img/hugo-icon-header.png`} />
                      <img className='img-dark-theme' width="18px" aria-hidden="true" src={`${baseUrl}/img/hugo-icon-header-dark.png`} />
                      <span className="text">Wiki</span>
                    </button>
                    <Tooltip id="botao-tooltip-3" place="bottom" effect="solid" className="tooltip-ds">
                      <span>Wiki</span>
                    </Tooltip>
                  </div>
                </div>
              </div>
              <Tooltip id="botao-tooltip-6" place="bottom" effect="solid" className="tooltip-ds">
                <span>Nossos Canais</span>
              </Tooltip>

              <span className="br-divider vertical mx-half mx-sm-1"></span>
              <div onFocus={(e) => setShowSearchTab(true)} id="main-searchbox" tabIndex="0" className="header-search-trigger" aria-label="Busca do site">
                <button
                  id="btSearch"
                  data-tip
                  data-tooltip-id="botao-tooltip-4"
                  className="br-button primary large circle ml-3"
                  type="button"
                  aria-label="Abrir Busca"
                  data-toggle="search"
                  data-target=".header-search"
                  onClick={() => {
                    setShowSearchTab(true)
                  }}
                >
                  <i className="fas fa-search" aria-hidden="true"></i>
                </button>

                <Tooltip id="botao-tooltip-4" place="bottom" effect="solid" className="tooltip-ds">
                  <span>Busca</span>
                </Tooltip>
              </div>
            </div>
          </div>

          <div className="header-bottom">
            <div className="header-menu">
              <div className="header-menu-trigger">
                <button
                  tabIndex={1}
                  data-tip
                  data-tooltip-id="botao-tooltip-5"
                  className="br-button large circle"
                  type="button"
                  aria-label="Abrir menu principal"
                  data-toggle="menu"
                  data-target="#main-navigation"
                  id="menu-compact"
                >
                  <i className="fas fa-bars" aria-hidden="true"></i>
                </button>
                <Tooltip id="botao-tooltip-5" place="bottom" effect="solid" className="tooltip-ds">
                  <span>Menu Principal</span>
                </Tooltip>
              </div>
              <div className="header-info">
                <div className="header-title">
                  <a
                    href="#"
                    onClick={(e) => {
                      e.preventDefault()
                      navigate(`/home`)
                    }}
                  >
                    {title}
                  </a>
                  <span className="br-tag bg-magenta-50 ml-3">{tagtext}</span>
                </div>
              </div>
            </div>

            {showSearchTab &&
              <div className={`header-search ${showSearchTab && 'active'}`}>
                <div className="br-input has-icon">
                  <Search showSearch={showSearchTab} setShow={setShowSearchTab} icon={false} />
                </div>
                <button
                  id="btCloseSearch"
                  onClick={() => setShowSearchTab(false)}
                  data-tip
                  data-tooltip-id="botao-tooltip-7"
                  className="br-button primary large ml-3 circle search-close"
                  type="button"
                  aria-label="Fechar Busca"
                  data-dismiss="search"
                >
                  <i className="fas fa-times" aria-hidden="true"></i>
                </button>
                <Tooltip id="botao-tooltip-7" place="bottom" effect="solid" className="tooltip-ds">
                  <span>Fechar Busca</span>
                </Tooltip>
              </div>
            }
          </div>
        </div>
      </header >
    </div>
  )
}
