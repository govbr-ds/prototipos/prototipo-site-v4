import { useState } from 'react';
import './footer.css';

export const Footer = () => {

  const baseUrl = process.env.PUBLIC_URL;
  const [focusFooter, setFocusFooter] = useState(false)

  let link1 = (
    <a href="https://creativecommons.org/publicdomain/zero/1.0/" target="_blank">
      CC0 1.0 Universal
    </a>
  )
  let link2 = (
    <a href="https://mit-license.org/" target="_blank">
      MIT
    </a>
  )
  let license = (
    <>
      O Padrão Digital de Governo utiliza as licenças {link1} e {link2}.
    </>
  )

  return (
    <footer id="footer" role="contentinfo" className={`br-footer inverted mb-4 ${focusFooter ? 'focus' : ' '}`}>
      <h2 onFocus={() => { setFocusFooter(true) }} onBlur={() => { setFocusFooter(false) }} id="footer-title" tabIndex={0}>Rodapé da página</h2>
      <div className="container-lg">
        <div className="logo flex justify-center py-4">
          <img className='img-light-theme' src={`${baseUrl}/img/logo-govbr.png`} alt="Logo Govbr" />
          <img className='img-dark-theme' src={`${baseUrl}/img/logo-govbr-dark.png`} alt="Logo Govbr" />
        </div>
      </div>
      <span className="br-divider"></span>
      <div className="container-lg">
        <div className="info flex justify-center pb-3">
          <div>{license}</div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
