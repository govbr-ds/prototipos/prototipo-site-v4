import '@govbr-ds/core/dist/core.min.css';
import Cookies from 'js-cookie';
import { useState } from 'react';
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';

//Data
import fundamentalsRoutes from './data/routes';

//Components
import Footer from 'components/Footer/Footer';
import { HeaderCompact } from 'components/Header/HeaderCompact';
import { Menu } from 'components/Menu/Menu';
import SkipLink from 'components/SkipLink/SkipLink';

//Templates
import HomeTemp from 'templates/HomeTemplate';
import ColumnTabTemp from './templates/ColumnTabTemplate';
import ColumnTemp from './templates/ColumnTemplate';
import ComingSoon from './templates/ComingSoon';
import PageNotFound from './templates/NotFound';
import SingleTemp from './templates/SingleTemplate';

//CSS
import './assets/styles/font.css';
import './assets/styles/index.css';

const App = () => {
  const location = useLocation()

  const statusMessage = Cookies.get('govbr-status-message');
  const [showMessage, setShowMessage] = useState(statusMessage !== 'false')


  const isHomeRoute = location.pathname === '/' || location.pathname === '/home'

  return (
    <>
      <SkipLink />
      <Menu />
      <HeaderCompact isHome={isHomeRoute} message={showMessage} setShowMessage={setShowMessage} />
      <main className={`${isHomeRoute ? 'home' : ''} ${showMessage ? 'show-message' : ''}`} id="main">
        <div className={isHomeRoute ? 'bg-home' : ''}>
          <div className="container-lg">
            <div id="main-content" className="main-content" aria-label="Conteúdo da página">
              <Routes>
                <Route path="/" element={<Navigate to={"/home"} />} />
                <Route path="/home" element={<HomeTemp />} />
                <Route path="/padroes" element={<SingleTemp title="Padrões" doc="padroes" />} />
                <Route path="/componentes" element={<SingleTemp title="Componentes" doc="componentes" />} />
                <Route path="/fundamentos" element={<SingleTemp title="Fundamentos" doc="fundamentos" />} />
                <Route
                  path="/introducao/como-comecar"
                  element={<ColumnTemp title="Como Começar" type="pages" doc="comocomecar" file="como-comecar.md" message={showMessage} />}
                />
                <Route
                  path="/introducao/fluxo-criacao"
                  element={<ColumnTemp title="Fluxo de Criação" type="pages" doc="comocomecar" file="criacao.md" message={showMessage} />} />
                <Route path="/introducao/sobre" element={<ColumnTemp title="Sobre o Padrão Digital de Governo" type="pages" doc="introducao" file="sobre.md" message={showMessage} />}
                />
                <Route
                  path="/introducao/fluxo-desenvolvimento"
                  element={<ColumnTemp title="Fluxo de Desenvolvimento" type="pages" doc="comocomecar" file="desenvolvimento.md" message={showMessage} />}
                />
                <Route
                  path="/introducao/principios"
                  element={<ColumnTemp title="Princípios e Valores do Design System" type="pages" doc="introducao" file="principios.md" message={showMessage} />}
                />
                {fundamentalsRoutes.map((item, index) => {
                  return <Route key={index} path={item.path} element={<ColumnTabTemp url={item.url} type={item.type} doc={item.doc} message={showMessage} />} />
                })}

                <Route path="*" element={<PageNotFound message="Página Não Encontrada" />} />
                <Route path="/fundamentos/addlink" element={<ComingSoon message="Conteúdo em Desenvolvimento" />} />
                <Route path="/padroes/addlink" element={<ComingSoon message="Conteúdo em Desenvolvimento" />} />
                <Route path="/componentes/addlink" element={<ComingSoon message="Conteúdo em Desenvolvimento" />} />
                <Route path="/addlink" element={<ComingSoon message="Conteúdo em Desenvolvimento" />} />

              </Routes>
            </div>
          </div>
        </div>

      </main>
      <Footer />
    </>
  )
}

export default App
