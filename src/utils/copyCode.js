export function copyCode() {
    // Seletor dos botões de cópia
    const copyButtons = document.querySelectorAll('.btCopyCode');

    // Adiciona um ouvinte de evento de clique a cada botão
    copyButtons.forEach((copyButton) => {
        copyButton.addEventListener('click', () => {
            // Encontra o ancestral comum (div.container-bt)
            const container = copyButton.closest('.container-bt');

            // Verifica se o ancestral comum foi encontrado
            if (container) {
                // Procura o elemento <pre><code> dentro do ancestral comum
                const codeElement = container.nextElementSibling.querySelector('pre code');

                // Verifica se o elemento <code> foi encontrado
                if (codeElement) {
                    // Cria uma área de transferência temporária
                    const tempTextarea = document.createElement('textarea');
                    tempTextarea.value = codeElement.textContent;

                    // Adiciona a área de transferência temporária ao DOM
                    document.body.appendChild(tempTextarea);

                    // Seleciona e copia o conteúdo da área de transferência temporária
                    tempTextarea.select();
                    document.execCommand('copy');

                    // Remove a área de transferência temporária do DOM
                    document.body.removeChild(tempTextarea);

                    // Adiciona a classe de sucesso ao botão e altera o ícone
                    copyButton.classList.add('success');
                    const spanElement = copyButton.querySelector('span');
                    if (spanElement) {
                        spanElement.textContent = 'Copiado';
                    }

                    const iconElement = copyButton.querySelector('i');
                    if (iconElement) {
                        iconElement.classList.remove('fa-copy');
                        iconElement.classList.add('fa-check');
                    }

                    // Após 4 segundos, remove a classe de sucesso e reverte o ícone
                    setTimeout(() => {
                        copyButton.classList.remove('success');
                        if (spanElement) {
                            spanElement.textContent = 'Copiar';
                        }

                        if (iconElement) {
                            iconElement.classList.remove('fa-check');
                            iconElement.classList.add('fa-copy');
                        }
                    }, 4000);

                    // Pode adicionar feedback adicional aqui, como uma mensagem de sucesso
                    console.log('Conteúdo copiado para a área de transferência!');
                } else {
                    console.error('Elemento <code> não encontrado para copiar.');
                }
            } else {
                console.error('Ancestral comum não encontrado.');
            }
        });
    });
}
