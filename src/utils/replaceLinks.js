export default function replaceLinks() {
    const baseUrl = process.env.PUBLIC_URL
    const links = document.querySelectorAll("a");


    links.forEach(link => {
        let href = link.getAttribute("href");

        if (href && !href.startsWith("http") && !href.startsWith("www") && !href.startsWith(baseUrl)) {
            href = href.replace(/^/, baseUrl);
            link.setAttribute("href", href);
        }
    });
}
