import { feedbackService } from "axiosConfig";

export default async function sendFeedback(data) {
    const requestBody = {
        session: data.session,
        like: data.like > 0 ? true : false,
        document: window.location.href,
        comentario: data.comentario,
        question: data.question,
    }

    try {
        const { data: responseData } = await feedbackService.post("feed/", { ...requestBody })
        if (responseData) {
            console.log('Resposta da API:', data);
        }
        return responseData
    }
    catch ({ request: { status } }) {
        throw new Error(`Erro na requisição: ${status}`);
    }
}
