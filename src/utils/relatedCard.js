//Card de Assuntos Relacionados e Blockquotes
export default function relatedCard() {
  const blockquotes = Array.from(document.querySelectorAll('blockquote'))

  blockquotes.forEach((blockquote) => {
    if (
      blockquote.textContent.startsWith('$link') ||
      blockquote.textContent.startsWith('$link-externo') ||
      blockquote.textContent.startsWith('$hashtag')
    ) {
      let meuTitulo = null
      let meuLink = null
      let meuTexto = null
      let descricao = null
      let titleLink = null
      const linkEl = blockquote.querySelector('a')

      if (linkEl) {
        meuTitulo = linkEl.textContent
        meuLink = linkEl.getAttribute('href')
        titleLink = linkEl.getAttribute('title')
        linkEl.parentNode.removeChild(linkEl)
      }
      const paragraphEl = blockquote.querySelector('p')

      if (blockquote.textContent.startsWith('$link-externo')) {
        descricao = paragraphEl.innerText.replace('$link-externo', '')
        meuTexto = descricao
      } else if (blockquote.textContent.startsWith('$link')) {
        descricao = paragraphEl.innerText.replace('$link', '')
        meuTexto = descricao
      } else if (blockquote.textContent.startsWith('$hashtag')) {
        descricao = paragraphEl.innerText.replace('$hashtag', '')
        meuTexto = descricao
      }

      const div = document.createElement('div')

      if (titleLink) {
        if (titleLink.includes("$small")) {
          div.className = 'related-card small'
        }
      } else {
        div.className = 'related-card'
      }

      const a = document.createElement('a')
      a.href = meuLink

      const divIcon = document.createElement('div')
      const icon = document.createElement('i')

      if (blockquote.textContent.startsWith('$link-externo')) {
        a.target = '_blank'
        icon.className = 'fas fa-external-link-alt fa-lg'
      } else if (blockquote.textContent.startsWith('$link')) {
        a.target = '_self'
        icon.className = 'fas fa-link fa-lg'
      } else if (blockquote.textContent.startsWith('$hashtag')) {
        a.target = '_self'
        icon.className = 'fas fa-hashtag fa-lg'
      }

      icon.setAttribute('aria-hidden', 'true')
      divIcon.appendChild(icon)

      const divContent = document.createElement('div')
      const label = document.createElement('label')
      label.textContent = meuTitulo
      const p = document.createElement('p')
      p.textContent = meuTexto

      divContent.appendChild(label)
      divContent.appendChild(p)

      a.appendChild(divIcon)
      a.appendChild(divContent)
      div.appendChild(a)

      blockquote.parentNode.replaceChild(div, blockquote)
    } else {
      const paragrafo = blockquote.querySelector('p')

      if (paragrafo) {
        let texto = paragrafo.innerHTML.trim()

        const padrao = /^\$([\w-]+)/ // Verifica se existe $ no início
        const correspondencia = texto.match(padrao)
        let palavraComDolar = null

        if (correspondencia) {
          palavraComDolar = correspondencia[1]
          texto = texto.replace(padrao, '').trim()
          paragrafo.innerHTML = texto
        }

        switch (palavraComDolar) {
          case 'nao-esqueca':
            inserirConteudo('Não esqueça', 'nao-esqueca')
            break
          case 'porque-e-importante':
            inserirConteudo('Por que é importante', 'porque-e-importante')
            break
          case 'va-mais-longe':
            inserirConteudo('Vá mais longe', 'va-mais-longe')
            break
          case 'moral-da-historia':
            inserirConteudo('Moral da história', 'moral-da-historia')
            break
          case 'assim-que-se-faz':
            inserirConteudo('Assim que se faz', 'assim-que-se-faz')
            break
          case 'faca':
            inserirConteudo('Faça', 'faca')
            break
          case 'recomendado':
            inserirConteudo('Recomendado', 'recomendado')
            break
          case 'nao-recomendado':
            inserirConteudo('Não Recomendado', 'nao-recomendado')
            break
          case 'nao-faca':
            inserirConteudo('Não Faça', 'nao-faca')
            break
          default:
            break
        }

        function inserirConteudo(textoTitulo, className) {
          const title = document.createElement('h3')
          title.textContent = textoTitulo
          blockquote.className = `fas ${className}`
          blockquote.insertBefore(title, paragrafo)
        }
      }
    }
  })
}
