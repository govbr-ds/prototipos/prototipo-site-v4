// adiciona primeira letra maiuscula no breadcrumbs
function capitalizeFirstLetter(word) {
  return word.charAt(0).toUpperCase() + word.slice(1)
}

// remove a classe active no componente tab
function removeActiveClass() {
  const itensTab = document.querySelectorAll('.tab-item')
  const itensPanel = document.querySelectorAll('.tab-panel')
  itensTab.forEach(function (itemTab) {
    itemTab.classList.remove('active')
  })
  itensPanel.forEach(function (itemPanel) {
    itemPanel.classList.remove('active')
  })
}

// FORÇA RECARREGAR A PÁGINA NA MUDANÇA DE # NA URL
function handleHashChange() {
  location.reload()
}

// ADICIONA RESPONSIVIDADE NAS TABELAS
function addClassWideTable() {
  var tables = document.querySelectorAll('table');

  var container = document.querySelector('.tab-content');
  var computedStyle = window.getComputedStyle(container);
  var paddingLeft = parseInt(computedStyle.getPropertyValue('padding-left'));
  var paddingRight = parseInt(computedStyle.getPropertyValue('padding-right'));
  var widthContainer = parseInt(computedStyle.getPropertyValue('width'));
  var paddingContainer = paddingLeft + paddingRight;

  tables.forEach(function (table) {
    if (((table.offsetWidth + paddingContainer) - 1) > widthContainer) {
      table.classList.add('wide-table');
    } else {
      table.classList.remove('wide-table');
    }
  });
}

// FUNÇÃO VOLTAR AO TOPO
const backToTheTop = () => {
  window.scrollTo({
    top: 700,
  })
}

//AJUSTA ALTURA DE IMAGENS FAÇA E NÃO FAÇA

function setHeight() {
  const paresClasses = [
    { imgClasse: "img-faca", naoImgClasse: "img-nao-faca" },
    { imgClasse: "img-recomendado", naoImgClasse: "img-nao-recomendado" }
  ];

  paresClasses.forEach(par => {
    let imgDivs = document.querySelectorAll(`.${par.imgClasse}`);

    imgDivs.forEach(imgDiv => {
      let alturaImg = imgDiv.offsetHeight;
      let proximaDiv = imgDiv.nextElementSibling;

      while (proximaDiv && !proximaDiv.classList.contains(par.naoImgClasse)) {
        proximaDiv = proximaDiv.nextElementSibling;
      }

      if (proximaDiv) {
        proximaDiv.style.height = (alturaImg - 1) + 'px';
      }
    });
  });
}

export { addClassWideTable, backToTheTop, capitalizeFirstLetter, handleHashChange, removeActiveClass, setHeight }

