// formata as imagens nas diretrizes
export default function replaceImage() {
  const tabContent = document.querySelector('.container-img')

  // FORMATA OS VÍDEOS
  function createVideoContainer(img, isLateral, isLoop, hasControl, hasAutoPlay, hasMuted, hasPlaysInline) {
    const videoContainer = document.createElement('div')

    videoContainer.className = 'img-block'

    if (isLateral) {
      videoContainer.className = 'img-block horizontal'
    }

    const video = document.createElement('video')
    video.setAttribute('width', '100%')
    video.setAttribute('height', '240')

    if (isLoop) {
      video.setAttribute('loop', '')
    }

    if (hasControl) {
      video.setAttribute('controls', '')
    }

    if (hasAutoPlay) {
      video.setAttribute('autoplay', '')
    }

    if (hasMuted) {
      video.setAttribute('muted', '')
    }

    if (hasPlaysInline) {
      video.setAttribute('playsinline', '')
    }

    const source = document.createElement('source')
    source.setAttribute('src', img.src)
    source.setAttribute('type', 'video/mp4')

    video.appendChild(source)
    video.appendChild(document.createTextNode('Desculpe, seu navegador não é compatível com vídeo em HTML5. '))

    const videoLink = document.createElement('a')
    videoLink.setAttribute('href', img.src)
    videoLink.innerText = 'Acessar o vídeo'
    videoLink.setAttribute('target', '_blank')
    video.appendChild(videoLink)

    const description = document.createElement('em')
    const nextEl = img.nextElementSibling

    if (nextEl && nextEl.tagName.toLowerCase() === 'em') {
      description.innerText = nextEl.textContent
      nextEl.parentNode.removeChild(nextEl)
    }

    videoContainer.appendChild(video)
    videoContainer.appendChild(description)

    return videoContainer
  }

  if (tabContent) {
    const images = tabContent.querySelectorAll('img')

    images.forEach((img) => {
      const testeTitle = img.getAttribute('title')
      const isInTable = img.closest('table')
      const isLink = img.closest('a')
      const isInUl = img.closest('ul')
      // const isInBlockquote = img.closest('blockquote')
      const container = img.closest('.img-block')
      const containerNotTest = img.closest('.img-not')
      let isNot = false

      // IMAGENS QUE NÃO SERÃO FORMATADAS 
      if (testeTitle) {
        isNot = testeTitle.includes("$not")
        if (isNot) {
          const imgCloneNot = img.cloneNode(true)
          imgCloneNot.setAttribute('title', testeTitle.replace('$not', '').trim())
          let newTitleNot = imgCloneNot.getAttribute('title')
          if (newTitleNot == '') {
            imgCloneNot.removeAttribute('title')
          }
          const containerNot = document.createElement('span')
          containerNot.className = 'img-not'
          containerNot.appendChild(imgCloneNot)
          img.parentNode.replaceChild(containerNot, img)
        }
      }

      // IMAGENS FORMATADAS
      if (!isInTable && !isInUl && !container && !isLink && !isNot && !containerNotTest) {
        const titleVideo = img.getAttribute('title')

        if (titleVideo && titleVideo.startsWith(`'$video`)) {
          const titleVideoFinal = titleVideo.replace(/^'(.*)'$/, '$1')
          let isLateral = titleVideoFinal.includes('$lateral')
          let isLoop = titleVideoFinal.includes('$loop')
          let hasControl = titleVideoFinal.includes('$controls')
          let hasAutoPlay = titleVideoFinal.includes('$autoplay')
          let hasMuted = titleVideoFinal.includes('$muted')
          let hasPlaysInline = titleVideoFinal.includes('$playsinline')

          const videoContainer = createVideoContainer(img, isLateral, isLoop, hasControl, hasAutoPlay, hasMuted, hasPlaysInline)

          img.parentNode.replaceChild(videoContainer, img)
        } else {
          const container = document.createElement('div')
          container.className = 'img-block'

          const imgClone = img.cloneNode(true)
          const title = img.getAttribute('title')

          let hasDoIt = false

          if (title) {
            const titleText = title.replace(/^'(.*)'$/, '$1')
            if (titleText && titleText.startsWith('$lateral')) {
              container.classList.add('horizontal')
              imgClone.setAttribute('title', titleText.replace('$lateral', '').trim())
              let newTitle = imgClone.getAttribute('title')
              if (newTitle == '') {
                imgClone.removeAttribute('title')
              }
            }
            // faça / não faça
            if (titleText && titleText.startsWith('$faca')) {
              hasDoIt = true
              container.classList.add('img-faca')
              imgClone.setAttribute('title', titleText.replace('$faca', '').trim())
              let newTitle = imgClone.getAttribute('title')
              if (newTitle == '') {
                imgClone.removeAttribute('title')
              }
            }
            if (titleText && titleText.startsWith('$nao-faca')) {
              hasDoIt = true
              container.classList.add('img-nao-faca')
              imgClone.setAttribute('title', titleText.replace('$nao-faca', '').trim())
              let newTitle = imgClone.getAttribute('title')
              if (newTitle == '') {
                imgClone.removeAttribute('title')
              }
            }
            if (titleText && titleText.startsWith('$recomendado')) {
              hasDoIt = true
              container.classList.add('img-recomendado')
              imgClone.setAttribute('title', titleText.replace('$recomendado', '').trim())
              let newTitle = imgClone.getAttribute('title')
              if (newTitle == '') {
                imgClone.removeAttribute('title')
              }
            }
            if (titleText && titleText.startsWith('$nao-recomendado')) {
              hasDoIt = true
              container.classList.add('img-nao-recomendado')
              imgClone.setAttribute('title', titleText.replace('$nao-recomendado', '').trim())
              let newTitle = imgClone.getAttribute('title')
              if (newTitle == '') {
                imgClone.removeAttribute('title')
              }
            }
          }

          container.appendChild(imgClone)

          const nextElement = img.nextElementSibling

          if (nextElement && nextElement.tagName === 'EM') {
            if (!hasDoIt) {
              const emClone = nextElement.cloneNode(true)
              container.appendChild(emClone)
              nextElement.parentNode.removeChild(nextElement)
            } else {
              nextElement.classList.add('emBlock')
            }

          }

          img.parentNode.replaceChild(container, img)

          if (hasDoIt) {
            let paragraphParent = container.parentElement;
            if (paragraphParent.tagName === 'P') {
              paragraphParent.classList.add('p-block');
            }
          }
        }
      }
    })
  }
}
