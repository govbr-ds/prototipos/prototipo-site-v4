interface TabRenderProps {
  panel: string,
  name: string,
  activeTabPanel: string
  onClick: () => void

}

const TabRender = ({ panel, name, activeTabPanel,onClick }: TabRenderProps) => {
  const activeTab = activeTabPanel === panel ? "active" : ""
  if (name == '') {
    return name
  }
  return (
    <li id={`item-${panel}`} onClick={onClick} className={`tab-item ${activeTab}`}>
      <button type="button" data-panel={panel}>
        <span className="name">
          <span className="d-flex flex-column flex-sm-row">
            <span className="name">{name}</span>
          </span>
        </span>
      </button>
    </li>
  )

}

export default TabRender
