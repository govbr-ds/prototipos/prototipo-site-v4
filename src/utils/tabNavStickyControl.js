// Em um novo arquivo, por exemplo, scrollUtils.js
export function handleScrollSticky(setIsSticky) {
    const handleScroll = () => {
        const navElement = document.querySelector('.tab-content')
        const navOffset = navElement.offsetTop
        const currentScroll = window.pageYOffset
        const offsetThreshold = 200

        if (currentScroll >= navOffset - offsetThreshold) {
            setIsSticky(true)
        } else {
            setIsSticky(false)
        }
    }

    window.addEventListener('scroll', handleScroll)

    return () => {
        window.removeEventListener('scroll', handleScroll)
    }
}
