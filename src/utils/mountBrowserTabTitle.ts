type MountBrowserTabTitleProps = {
    breadcrumbData: Array<{ label: string, url: string }> | string
}

const MountBrowserTabTitle = ({ breadcrumbData }: MountBrowserTabTitleProps) => {
    if (typeof breadcrumbData === "string") {
        document.title = `Padrão Digital de Governo - V4`
    }
    else {
        const reduceTitle = breadcrumbData.reduce((acc: string, currentItem: { label: string, url: string }) => {
            if (currentItem.label) {
                acc += currentItem.label.charAt(0).toLocaleUpperCase() + currentItem.label.slice(1) + " "
            }
            return acc
        }, "")
        const updatedTitle = `Padrão Digital de Governo - V4 ${reduceTitle}`
        document.title = updatedTitle
    }
}

export { MountBrowserTabTitle }


