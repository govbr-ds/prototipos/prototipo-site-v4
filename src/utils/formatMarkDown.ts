const formatMarkdown = (markdown: string, baseUrl: string, path: string): { content: string, metadata?: Record<string, string> } => {


    const imgPath = `${baseUrl}/${path}imagens/`
    const videoPath = `${baseUrl}/${path}videos/`
    const newDataImage = markdown.replace(/imagens\//g, `${imgPath}`)
    const newDataVideo = newDataImage.replace(/videos\//g, `${videoPath}`)

    // insere tag vazia antes dos blockquotes
    const newDataBlockquote = newDataVideo.replace(/^> \$(.*)/gm, '<!-- -->\n$&')

    // insere tag vazia antes de links com imagem
    const newDataLinkImage = newDataBlockquote.replace(/\[!\[(.*?)\]\((.*?)\)\]\((.*?)\)/gm, '<!-- -->\n$&');

    // Adicione o botão copiar antes de <pre>
    const newDataFinal = newDataLinkImage.replace(/```(html|bash|javascript)/g, '<div className="container-bt"><button className="br-button btCopyCode" type="button" aria-label="Ícone ilustrativo"><i class="fas fa-copy" aria-hidden="true"></i> <span>Copiar</span></button></div>\n```$1');

    const frontMatterRegex = /^---\s*([\s\S]*?)\s*---/g;
    const hasMetadata = newDataFinal.matchAll(frontMatterRegex).next()
    if (hasMetadata.value) {
        const formatedContent = newDataFinal.replace(frontMatterRegex, '')
        const metadatas = hasMetadata.value[1].split("\n")
        const metadata = metadatas.reduce((acc: Record<string, string>, currentItem: string) => {
            const [key, value] = currentItem.split(":")
            acc = { ...acc, [key]: value }
            return acc
        }, {})
        return { content: formatedContent, metadata }
    }

    return { content: newDataFinal }
}


export { formatMarkdown }

