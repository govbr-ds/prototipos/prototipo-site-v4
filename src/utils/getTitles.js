// CAPTURA TÍTULOS H2 NOS ARQUIVOS MARKDOWN PARA USAR NO MENU CONTEXTUAL
export function getTitles(tabId, setSelectedList) {
    const minhaDiv = document.getElementById(tabId);

    if (minhaDiv) {
        const h3Elements = Array.from(minhaDiv.getElementsByTagName('h2'));
        const h3Values = h3Elements.map((element) => element.textContent || '');

        setSelectedList(h3Values);
    } else {
        setSelectedList(null);
    }
}

// CAPTURA TÍTULOS H2 NOS ARQUIVOS MARKDOWN PARA USAR NO MENU CONTEXTUAL | CONSIDERANDO AS ABAS
export function getTabTitles(tabId, setSelectedList) {
    const minhaDiv = document.getElementById(tabId);

    // Captura o texto do primeiro item
    var firstItemElement = document.querySelector(`#item-${tabId} .name`) || null;
    var firstItem = firstItemElement ? firstItemElement.textContent : null;

    if (minhaDiv) {
        // Captura todos os títulos h2 dentro da aba selecionada
        const h3Elements = Array.from(minhaDiv.getElementsByTagName('h2'));
        const h3Values = h3Elements.map((element) => element.textContent || '');

        // Define a lista com o primeiro item e os valores dos títulos h2
        setSelectedList([firstItem, ...h3Values]);
    } else {
        setSelectedList(null);
    }
}