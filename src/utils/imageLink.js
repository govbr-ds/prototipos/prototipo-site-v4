
function addHttp(texto) {
    // Verifica se o texto começa com www seguido por um ponto e caracteres
    if (/^www\.\S+/i.test(texto)) {
        return 'http://' + texto;
    }
    // Se não começar com www, retorna o texto original
    return texto;
}

export default function imageLink() {
    const links = document.querySelectorAll('.container-img a');

    links.forEach(link => {
        const linkContent = link.textContent.trim();
        if (linkContent.startsWith('![')) {
            const imgSrc = link.getAttribute('href');
            const imgTitle = link.getAttribute('title');
            const imglegend = linkContent.replace('![', '').trim();
            const nextLink = link.nextElementSibling;
            let url = null;

            // verifica se existe um próximo link ou texto entre parêntesis
            if (nextLink && nextLink.tagName.toLowerCase() === 'a') {
                url = nextLink.getAttribute('href');
            } else {
                const afterLink = link.nextSibling;
                if (afterLink) {
                    const textAfterLink = afterLink.textContent.trim();
                    const textBetweenParentheses = textAfterLink.match(/\(([^)]+)\)/);
                    if (textBetweenParentheses && textBetweenParentheses.length > 1) {
                        url = textBetweenParentheses[1]
                    }
                }
            }

            // Encontra o elemento pai <p>
            const paragraph = link.closest('p');

            if (paragraph) {
                // Cria a imagem
                const newImg = document.createElement('img');
                newImg.setAttribute('src', imgSrc);
                newImg.setAttribute('title', imgTitle);
                newImg.setAttribute('alt', imglegend);

                // Cria o link
                const newLink = document.createElement('a');
                newLink.setAttribute('href', addHttp(url));
                newLink.appendChild(newImg);

                // Substitui a estrutura antiga pela nova
                paragraph.innerHTML = '';

                paragraph.appendChild(newLink);
            }
        }
    });
}
