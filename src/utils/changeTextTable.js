export function changeTextTable() {
    var container = document.getElementById('panel-3');

    if (container) {
        var tabelas = container.getElementsByTagName('table');

        for (var i = 0; i < tabelas.length; i++) {
            var celulas = tabelas[i].getElementsByTagName('td');

            for (var j = 0; j < celulas.length; j++) {
                // Verifica se a célula tem apenas um filho que é um nó de texto
                if (celulas[j].childNodes.length === 1 && celulas[j].childNodes[0].nodeType === Node.TEXT_NODE) {
                    var texto = celulas[j].childNodes[0].nodeValue.trim();

                    if (texto !== '') {
                        var span = document.createElement('span');
                        span.textContent = texto;
                        celulas[j].innerHTML = '';
                        celulas[j].appendChild(span);
                    }
                }
            }
        }
    }
}