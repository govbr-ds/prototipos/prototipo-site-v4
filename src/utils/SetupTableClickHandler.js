export function setupTableClickHandler() {
  const container = document.getElementById('panel-4')
  const tooltip = document.getElementById('tooltipToken')

  if (container) {
    container.addEventListener('click', function (event) {
      const targetCell = event.target.closest('code')

      if (targetCell) {
        const content = targetCell.textContent.trim()
        const textArea = document.createElement('textarea')
        textArea.value = content
        document.body.appendChild(textArea)
        textArea.select()
        document.execCommand('copy')
        document.body.removeChild(textArea)

        //targetCell.classList.add('copied')
        tooltip.classList.add('success')

        setTimeout(() => {
          //targetCell.classList.remove('copied')
          //targetCell.classList.add('fade-out')
          tooltip.classList.remove('success')
        }, 2000)
      }
    })
  }
}

export function setTooltip() {
  const container = document.getElementById('panel-4');
  const tooltip = document.getElementById('tooltipToken');

  if (container && tooltip) {
    container.addEventListener('mousemove', function (e) {
      if (e.target.closest('code')) {
        const cellContent = e.target.closest('td');
        if (cellContent !== null) {
          const cellContentInnerHTML = cellContent.innerHTML
          const isImage = cellContentInnerHTML.includes('<img');

          if (!isImage) {
            tooltip.classList.add('active');

            const offsetX = 10;
            const offsetY = 10;

            const mouseX = e.clientX + offsetX;
            const mouseY = e.clientY + offsetY;

            tooltip.style.position = 'fixed';
            tooltip.style.left = mouseX - 80 + 'px';
            tooltip.style.top = mouseY - 95 + 'px';
          } else {
            tooltip.classList.remove('active');
          }
        }


      }
    });

    container.addEventListener('mouseleave', function () {
      tooltip.classList.remove('active');
    });

    container.addEventListener('mouseover', function (e) {
      if (e.target.closest('#panel-3') && !e.target.closest('code')) {
        tooltip.classList.remove('active');
      }
    });
  }
}
