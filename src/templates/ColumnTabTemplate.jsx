import { useEffect, useRef, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

//components
import Breadcrumb from 'components/Breadcrumb/Breadcrumb';
import Feedback from 'components/Feedback/Feedback';
import StickyMenu from 'components/StickyMenu/StickyMenu';

//utils
import { changeTextTable } from 'utils/changeTextTable';
import imageLink from 'utils/imageLink';
import relatedCard from 'utils/relatedCard';
import replaceImage from 'utils/replaceImage';
import replaceLinks from 'utils/replaceLinks';
import MarkdownRenderer from '../components/MardownRenderer/markdownRender';
import { copyCode } from '../utils/copyCode.js';
import { getTabTitles } from '../utils/getTitles.js';
import { addClassWideTable, backToTheTop, setHeight } from '../utils/utils.js';
//styles
import { useTabs } from 'hooks/useTabs';
import { MountBrowserTabTitle } from 'utils/mountBrowserTabTitle';
import { handleScrollSticky } from 'utils/tabNavStickyControl';
import TabRender from 'utils/tabRender';
import '../assets/styles/blockquotes.css';
import '../assets/styles/images.css';
import '../assets/styles/relatedcards.css';
import '../assets/styles/states.css';
import '../assets/styles/tooltip.css';
import { setTooltip, setupTableClickHandler } from '../utils/SetupTableClickHandler';
import './styles/column-tab-template.css';

const ColumnTabTemplate = (props) => {
  const baseUrl = process.env.PUBLIC_URL
  const brTabRef = useRef()
  const [tabId, setTabId] = useState()
  const [click, setClick] = useState(false)
  const [selectedList, setSelectedList] = useState(null)
  const [isLoading, setIsLoading] = useState(true)
  const [isSticky, setIsSticky] = useState(false)
  const [findWordSearch, setFindWordSearch] = useState(false)
  const stickyClass = `${isSticky ? 'sticky' : ''}`
  const message = `${props.message ? 'show-message' : ''}`
  const navigate = useNavigate()
  const { hash, search } = useLocation()

  const location = useLocation();

  const getRouteClass = (pathname) => {
    if (pathname.includes('/componentes')) {
      return 'componentes';
    } else if (pathname.includes('/fundamentos')) {
      return 'fundamentos';
    } else if (pathname.includes('/padroes')) {
      return 'padroes';
    }
    return ''; // Retorna uma string vazia se nenhum dos casos for verdadeiro
  };

  let url = `docs/diretrizes/projects/diretrizes/${props.url}/${props.doc}/`
  let doc = `${props.doc}`
  let type = `${props.type}`
  const { tabs, activeTab, setActiveTab } = useTabs({ url: props.url, type, doc })

  // DADOS BREADCRUMB
  let breadcrumbData = [
    {
      label: `${type}`,
      url: `${baseUrl}/${props.url}`,
    },
    {
      label: `${doc}`,
    },
  ]

  // FUNÇÃO SETA ABAS ATIVAS | VOLTA PARA O TOPO AO CLICAR NAS ABAS
  const handleClick = (tab) => {
    navigate(`?${tab.docName}`)
    setActiveTab(tab)
    var scrollAtual = window.scrollY || window.pageYOffset
    if (scrollAtual >= 700) {
      backToTheTop()
    }
  }

  useEffect(() => {
    // CONTROLE DO STICKY NO TAB NAV
    const cleanupScroll = handleScrollSticky(setIsSticky);
    return () => {
      cleanupScroll();
    }
  }, [isSticky]);

  useEffect(() => {
    if (activeTab && breadcrumbData) {
      setTimeout(() => {
        MountBrowserTabTitle({ breadcrumbData: breadcrumbData })
        setFindWordSearch(false)
        getTabTitles(activeTab.panel, setSelectedList);
        replaceImage()
        relatedCard()
        imageLink()
        setTooltip()
        changeTextTable()
        replaceLinks()
        copyCode()
        setupTableClickHandler()
        addClassWideTable()
        Prism.highlightAll();
        setIsLoading(false)
      }, 1000)

      setTimeout(() => {
        setHeight()
        setIsLoading(false)
      }, 2000)

      setIsLoading(true)
    }
  }, [activeTab])

  useEffect(() => {
    const handleResize = () => {
      addClassWideTable();
    };

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    setTimeout(() => {
      if (activeTab && hash) {
        const panel = document.getElementById(activeTab.panel);
        const element = document.getElementById(hash.replace('#', ''));
        if (element) {
          element.classList.add('highlighttext')
          element.scrollIntoView()
          window.scrollBy(0, -150);
        }
      }
    }, 500);
  }, [hash, activeTab])

  return (
    <div className="fundamento-container">
      <div>
        <div className="breadcrumb-container py-4">
          <Breadcrumb data={breadcrumbData} />
        </div>
        <div className="doc-container">
          {activeTab && <MarkdownRenderer path={url} file="intro.md" />}
        </div>
        <div id="tabsDiretrizes" className={`br-tab ${getRouteClass(location.pathname)}`} ref={brTabRef}>
          <div className="container-tab-nav">
            <nav className={`tab-nav ${stickyClass} ${message}`} aria-label="Navegação por Abas">
              <ul className="container-lg">
                {tabs.map(tab => (
                  <TabRender
                    key={tab.panel}
                    onClick={() => {
                      handleClick(tab)
                    }}
                    panel={tab.panel}
                    name={tab.name}
                    activeTabPanel={activeTab.panel}
                  />
                ))}
                <li className="tab-item"></li>
              </ul>
            </nav>
          </div>

          <div className="tab-content container-img">
            {
              activeTab && <div className={`tab-panel active`} id={activeTab.panel} key={activeTab.docName}>
                <MarkdownRenderer path={url} file={`${activeTab.docName}.md`} />
              </div>
            }
          </div>
        </div>
      </div>
      <div className="columnMenu">
        <div id="contextualMenu" className={`contextualMenu sticky ${message}`}>

          {isLoading ? (
            <div className="containerLoading">
              <div className="loading medium"></div>
            </div>
          ) : (

            <StickyMenu tabId={activeTab.panel} url={url} activeTab={activeTab} selectedList={selectedList} hasTab={true} />

          )}
          <Feedback tabId={tabId} />
        </div>
      </div>
      <div id="tooltipToken">
        <i className='fas fa-check-circle'></i>
        <span className='textDefault'>Copiar para o clipboard</span>
        <span className='textCopied'>Copiado!</span>
      </div>
    </div>
  )
}

export default ColumnTabTemplate
