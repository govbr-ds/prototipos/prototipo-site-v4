import { useEffect, useState } from 'react';

//components
import Breadcrumb from 'components/Breadcrumb/Breadcrumb';
import Feedback from 'components/Feedback/Feedback';
import StickyMenu from 'components/StickyMenu/StickyMenu';

//utils
import replaceLinks from 'utils/replaceLinks';
import MarkdownRenderer from '../components/MardownRenderer/markdownRender';
import { copyCode } from '../utils/copyCode.js';
import { getTitles } from '../utils/getTitles.js';
import relatedCard from '../utils/relatedCard.js';
import replaceImage from '../utils/replaceImage.js';
import { setHeight } from '../utils/utils.js';

//styles
import { MountBrowserTabTitle } from 'utils/mountBrowserTabTitle';
import '../assets/styles/blockquotes.css';
import '../assets/styles/images.css';
import '../assets/styles/relatedcards.css';
import '../assets/styles/states.css';
import '../assets/styles/tooltip.css';
import './styles/column-template.css';


const ColumnTemplate = (props) => {

  let urlPart = 'diretrizes/'

  if (props.type == 'pages') {
    urlPart = ''
  }

  let url = `docs/${urlPart}${props.type}/${props.doc}/`

  const [isLoading, setIsLoading] = useState(true)
  const [tabId, setTabId] = useState('panel-1')
  const [selectedList, setSelectedList] = useState(null)
  const message = `${props.message ? 'show-message' : ''}`

  // DADOS BREADCRUMB
  let breadcrumbData = [
    {
      label: `${props.title}`,
    },
  ]

  useEffect(() => {
    setTimeout(() => {
      getTitles(tabId, setSelectedList);
      MountBrowserTabTitle({ breadcrumbData: breadcrumbData })
      replaceImage()
      relatedCard()
      replaceLinks()
      setHeight()
      copyCode()
      Prism.highlightAll();
      setIsLoading(false)
    }, 1000)
    setIsLoading(true)
  }, [])

  return (
    <>
      <div className="py-4">
        <Breadcrumb data={breadcrumbData} />
      </div>
      <div className="container-files container-img">
        <div id='panel-1'>
          <MarkdownRenderer path={url} file={props.file} />
        </div>
        <div className="columnMenu">
          <div id="contextualMenu" className={`contextualMenu sticky ${message}`}>
            {isLoading ? (
              <div className="containerLoading">
                <div className="loading medium"></div>
              </div>
            ) : (
              <StickyMenu tabId={tabId} selectedList={selectedList} hasTab={false} />
            )}
            <Feedback />
          </div>
        </div>
      </div>
    </>
  )
}

export default ColumnTemplate
