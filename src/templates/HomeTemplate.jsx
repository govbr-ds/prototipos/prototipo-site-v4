//components
import CardList from 'components/CardList/CardList';
import ChannelList from 'components/ChannelList/ChannelList';
import Hero from 'components/Hero/Hero';
import Carousel from '../components/Carousel/Carousel';

//styles
import { useEffect } from 'react';
import { MountBrowserTabTitle } from 'utils/mountBrowserTabTitle';
import '../assets/styles/states.css';
import './styles/home-template.css';

const HomeTemplate = () => {
  useEffect(() => {
    MountBrowserTabTitle({ breadcrumbData: "home" })
  }, [])
  return (
    <>
      <Hero />
      <CardList doc="highlights" heading="h3" maxItems={3} />
      <Carousel />
      <ChannelList />
    </>
  )
}

export default HomeTemplate
