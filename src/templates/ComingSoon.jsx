import { useNavigate } from 'react-router-dom'
import './styles/notfound.css'

export default function ComingSoon(props) {
    const baseUrl = process.env.PUBLIC_URL
    const navigate = useNavigate()
    return (
        <>
            <div className="notFoundContainer">
                <div className="message-container">
                    <img src={`${baseUrl}/img/coming-soon.png`} />
                    <div>
                        <h3>{props.message}</h3>
                        <p>Estamos trabalhando em algo incrível para você. Nossa equipe está dedicando tempo e esforço para criar uma experiência única e envolvente. Por favor, volte em breve para conferir as novidades.
                            Aproveite para conhecer nossa <a href="https://discord.gg/NkaVZERAT7" target='_blank'>comunidade</a> e dar <em>feedback</em> sobre este problema.
                        </p>
                    </div>
                </div>
                <div className="actions-container">
                    <button class="br-button" type="button" onClick={() => {
                        window.history.back()
                    }}>
                        <i className='fas fa-chevron-left'> </i> Ir para página anterior
                    </button>
                    <button class="br-button" type="button" onClick={() => {
                        navigate(`${baseUrl}/home`)
                    }}>
                        <i className='fas fa-home'> </i> Ir para página principal
                    </button>
                </div>
            </div>
        </>
    )
}

