import { useEffect, useState } from 'react';

//components
import Breadcrumb from 'components/Breadcrumb/Breadcrumb';
import CardList from 'components/CardList/CardList';

//data
import { dataIntro } from '../data/data';

//utils
import { capitalizeFirstLetter } from '../utils/utils.js';

//styles
import { MountBrowserTabTitle } from 'utils/mountBrowserTabTitle';
import '../assets/styles/states.css';
import './styles/single-template.css';

const SingleTemplate = (props) => {
  // DADOS BREADCRUMB
  let breadcrumbData = [
    {
      label: `${props.title}`,
    },
  ]

  const [valueFilter, setValueFilter] = useState('')

  const handleInputChange = (event) => {
    const inputValue = event.target.value
    setValueFilter(inputValue)
  }

  useEffect(() => {
    MountBrowserTabTitle({ breadcrumbData: breadcrumbData })
  }, [])

  return (
    <>
      <div className="py-4">
        <Breadcrumb data={breadcrumbData} />
      </div>
      <div className="container-intro container-img">
        <h1>
          {props.title} <small>| Introdução</small>
        </h1>
        <p>
          {dataIntro.map((item) => {
            if (capitalizeFirstLetter(props.title) == item.title) {
              return item.description;
            } else {
              return '';
            }
          })}
        </p>

        <hr className="my-8"></hr>
        <div className="br-input input-highlight mb-8">
          <div className="input-group">
            <div className="input-icon">
              <i className="fas fa-filter" aria-hidden="true"></i>
            </div>
            <input
              className="input-highlight"
              id="input-highlight-icon-search-labeless"
              type="search"
              placeholder={`Filtrar ${props.title}`}
              value={valueFilter}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <CardList doc={props.doc} maxItems={30} heading="h2" dataFiltered={valueFilter} />
      </div>
    </>
  )
}

export default SingleTemplate
