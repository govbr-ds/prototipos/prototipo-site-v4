import axios from "axios";

const baseUrl = process.env.PUBLIC_URL
const contentService = axios.create({
    baseURL: baseUrl,
    headers: {
        "Accept": 'application/json'
    }
})

const feedbackService = axios.create({
    baseURL: "https://dev-api-avaliacao-ds.estaleiro.serpro.gov.br/",

})

export { contentService, feedbackService };

