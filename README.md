# Readme

## Como rodar localmente

1. Baixar o projeto - `git clone git@gitlab.com:govbr-ds/tools/site/govbr-ds-site-react.git`
2. Baixar as dependências - `npm install`
3. Baixar os conteúdos - `git submodule init && git submodule update`
4. Rodar o projeto - `npm run start`

## Rodar o site com outra branch no submodules das diretrizes

1. No menu lateral esquerdo, selecione **Build** e depois **Pipelines**. Em seguida, clique no botão **Run Pipeline**. Para evitar problemas de cache, certifique-se de clicar em Clear Runner Caches.

2. Adicione a variável **DIRETRIZ_BRANCH** no campo **Input Variable Key** e insira o nome da branch no campo **Input Variable Value**. Lembre-se de que a branch deve ser criada a partir da branch "next" no repositório <https://gitlab.com/govbr-ds/govbr-ds>.

## Licença

This project is tested with [BrowserStack](https://www.browserstack.com/).



## Licença

This project is tested with [BrowserStack](https://www.browserstack.com/).
