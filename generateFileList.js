const fs = require('fs')
const path = require('path')

const diretorioFundamentos = path.join(__dirname, 'public', 'docs', 'diretrizes', 'projects', 'diretrizes', 'fundamentos')
const diretoriopadroes = path.join(__dirname, 'public', 'docs', 'diretrizes', 'projects', 'diretrizes', 'padroes')
const diretorioComponentes = path.join(__dirname, 'public', 'docs', 'diretrizes', 'projects', 'diretrizes', 'componentes')


let obj = { fundamentos: [], padroes: [], componentes: [] }


const files = fs.readdirSync(diretorioFundamentos)
if (files.length > 0) {
    const subfiles = files.map(subject => {
        const subjectPath = path.join(diretorioFundamentos, subject)
        const content = fs.readdirSync(subjectPath)
        return {
            [subject]: content.filter(c => c.includes('.md'))
        }
    })

    obj.fundamentos.push(...subfiles)
}

const filespadroes = fs.readdirSync(diretoriopadroes)
if (filespadroes.length > 0) {
    const subfiles = filespadroes.map(subject => {
        const subjectPath = path.join(diretoriopadroes, subject)
        const content = fs.readdirSync(subjectPath)
        return { [subject]: content.filter(c => c.includes('.md')) }
    })

    obj.padroes.push(...subfiles)
}

const filescomponentes = fs.readdirSync(diretorioComponentes)
if (filescomponentes.length > 0) {
    const subfiles = filescomponentes.map(subject => {
        const subjectPath = path.join(diretorioComponentes, subject)
        const content = fs.readdirSync(subjectPath)
        return { [subject]: content.filter(c => c.includes('.md')) }
    })

    obj.componentes.push(...subfiles)
}

fs.writeFile(path.join(__dirname, 'public', 'docs', 'tabs.json'), JSON.stringify(obj), err => {
    if (err) {
        console.log(error)
        return;
    }
    console.log('JSON criado com sucesso')
    console.log('diretoro salvo')
})





