# Princípios e valores do Design System do Governo do Brasil

## Princípios do Design System

São os fundamentos filosóficos e práticos que norteiam sua criação, desenvolvimento e aplicação. Eles servem como um conjunto de regras ou diretrizes que ajudam a manter a consistência e a qualidade do *design* em uma variedade de produtos digitais e plataformas.

### Objetivos dos Princípios

- Guiar decisões de *design*;
- Ser autênticos e específicos, em vez de genéricos;
- Ser facilmente memorizáveis, fáceis de lembrar e utilizáveis no dia a dia.

> $moral-da-historia
> Os princípios devem ser seguidos em todas as decisões de seu projeto. Porém, em casos específicos, e desde que haja claro benefício para os usuários, certas regras podem ser "quebradas". Reflita bastante antes de tomar essa decisão, realize testes de usabilidade e fundamente qualquer desalinhamento dos princípios.

A seguir, apresentamos os cinco princípios básicos do Design System:

### 1- Experiência única

Os diversos serviços digitais do governo se propõem a atender os cidadãos como uma central única que ofereça soluções para diferentes necessidades. Devem, portanto, compartilhar e seguir os mesmos padrões estruturais e visuais. A experiência precisa conferir confiança aos usuários, diminuindo sua curva de aprendizado. A interface deve ser consistente, porém flexível e adaptável de acordo com o contexto de quem a utiliza.

> $nao-esqueca
> Assegura que os elementos visuais e funcionais se comportem de maneira previsível em todas as aplicações para que os usuários não tenham que reaprender interações ou se confundam com diferentes estilos de *design*.

### 2- Eficiência e clareza

É importante que os processos dos serviços sejam simples e eficazes, com informações organizadas e estruturadas para solucionar de forma rápida e clara as necessidades dos usuários. Informações importantes e essenciais para o uso dos serviços devem ser sempre priorizadas. A eficiência precisa ser alcançada sem prejudicar a compreensão do usuário.

> $nao-esqueca
> Busca criar experiências de usuário que sejam compreensíveis e que permitam a realização de tarefas com facilidade, rapidez e sem ambiguidades

### 3- Acessibilidade

É fundamental assegurar que produtos e serviços possam ser usados por todas as pessoas, incluindo aquelas com deficiências ou limitações. Atendendo às principais orientações internacionais e o [Modelo de Acessibilidade em Governo Eletrônico (e-MAG)](https://emag.governoeletronico.gov.br/), este princípio enfatiza a importância de criar designs que sejam inclusivos e levem em consideração uma ampla gama de habilidades e circunstâncias dos usuários.

> $nao-esqueca
> Busca criar soluções que não apenas beneficiam indivíduos com deficiências, mas também melhoram a experiência de todos os usuários. A acessibilidade é uma questão de direito civil e um aspecto essencial do design responsável e ético.

### 4- Reutilização

Interfaces digitais de *websites*, aplicativos e sistemas de Governo devem, sempre que possível, utilizar componentes existentes. Caso haja necessidade, novos artefatos podem ser desenvolvidos, respeitando os padrões visuais e funcionais apresentados no Padrão Digital de Governo.

> $nao-esqueca
> Promove a reutilização de componentes e padrões para acelerar o desenvolvimento, reduzir o esforço e garantir a uniformidade em diferentes produtos e plataformas.

### 5- Colaboração

O aprimoramento contínuo dos padrões apresentados pelo Padrão Digital de Governo deve ser incremental, colaborativo e contínuo, baseado nas necessidades dos cidadãos e nas experiências já adquiridas. Neste sentido, o Padrão Digital de Governo promoveu a criação de uma comunidade ativa onde seus membros compartilham conhecimentos, fornecem *feedbacks* e trabalham de forma colaborativa a fim de evoluir consistentemente o Design System.

> $nao-esqueca
> Cria soluções coesivas e eficientes, que são sustentadas não apenas por diretrizes sólidas, mas também pelo compromisso e entrosamento das equipes.

---

## Valores do Design System

Alguns de nossos valores foram revistos com o objetivo de garantir um melhor atendimento a todos os cidadãos. Nossos valores moldam não apenas o que fazemos, mas como moldamos o futuro da experiência do usuário.

Estamos aqui para mais do que atender necessidades. Queremos inspirar e surpreender.

### 1- O usuário é mais importante que a tecnologia

Abraçamos a filosofia de colocar a atenção no coração de nossas decisões. Buscamos entendemos profundamente as necessidades dos nossos usuários antes de explorar as maravilhas da tecnologia. Acreditamos que a tecnologia se adapta aos objetivos, não o contrário. Em um mundo de possibilidades, buscamos diversas maneiras de resolver um único desafio.

### 2- Boa fé dos usuários como premissa

Acreditamos na boa fé dos nossos usuários como a fundação sólida das nossas decisões. Evitamos comprometer soluções simples e eficientes devido à má-fé. Nossos mecanismos de controle são empregados com cautela, minimizando impactos na experiência do usuário, enquanto mantemos a integridade da aplicação.

### 3- Compreensão além dos limites

Nosso compromisso é com a compreensão e compatibilidade, transcendendo barreiras entre humanos e máquinas. Nossos serviços visam ser tão claros para os usuários quanto para assistentes virtuais. Vemos a inteligência artificial como aliada, capacitando a classificação e organização de conteúdo de forma intuitiva e eficaz.
