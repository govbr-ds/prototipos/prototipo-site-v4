# Sobre o Padrão Digital de Governo

## Como tudo começou?

O projeto atual é uma evolução do trabalho desenvolvido para a construção da **Identidade Digital de Governo (IDG)** e da **IDG de Serviços**, iniciativas que buscavam padronizar os portais dos órgãos públicos federais para otimizar a comunicação e simplificar as interfaces de oferta de informações para o cidadão.

O diferencial do Design System do Governo do Brasil é possibilidade de realizar *downloads* de *templates*, códigos e componentes validados, propiciando o reaproveitamento dos elementos necessários para o desenvolvimento de interfaces, gerando maior eficiência e produtividade a todos os  envolvidos no projeto.

A iniciativa foi desenvolvida no âmbito do **Projeto de Unificação de Canais Digitais do Governo** com a participação da:

-   Secretaria de Governo Digital do Ministério da Economia (SGD);
-   Secretaria Especial de Modernização do Estado (SEME);
-   Secretaria Especial de Comunicação Social da Presidência da República (SECOM);
-   Serviço Federal de Processamento de Dados (SERPRO).

## O que é um *Design System*?

***Design System*** é um conjunto abrangente de padrões de *design*, práticas recomendadas, recursos e diretrizes que visam trazer consistência e eficiência ao processo de *design* e desenvolvimento de produtos digitais.

## O que é o Padrão Digital de Governo?

O **Padrão Digital de Governo** é o *Design System* do Governo Federal do Brasil. Você encontrará aqui dicas e padrões de interface que devem ser seguidos por *designers* e desenvolvedores para garantir a experiência única nos produtos do Governo.

> "Um Design System não é um projeto, é um produto que serve produtos." (Natan Curtis)

Desta forma, o Padrão Digital de Governo se propõe a ser um documento vivo com diversas diretrizes e componentes que servem como atalhos para garantir a eficiência na produção de produtos digitais e a garantir a padronização da experiência única para todos os usuários.

> $nao-esqueca
> Este documento está em contante evolução. Por isso, criamos uma comunidade onde qualquer pessoa pode participar, ajudando a melhorar cada vez mais o Design System. **O Design System é um produto colaborativo**.

## Por que unificar a experiência digital dos cidadãos brasileiros?

A proposta surgiu da necessidade de oferecer ao cidadão uma experiência única e consistente ao usar os serviços e produtos digitais do Governo. O objetivo é tornar mais simples e eficiente o uso das plataformas e sistemas governamentais. Assim, os usuários só precisam aprender uma vez como tudo funciona, e podem esperar que essa mesma lógica se aplique em todos os demais serviços.

## O Padrão Digital de Governo está alinhado com os princípios de acessibilidade e usabilidade?

Usabilidade é parte essencial da experiência e é percebida por meio de testes, *feedbacks* e análises de como os usuários interagem com o produto.

Um produto com boa usabilidade é acessível, eficaz e proporciona uma experiência agradável.

> $va-mais-longe
> **Acessibilidade** é parte integrante da inclusão digital e é frequentemente regulamentada por leis e diretrizes, como as [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/TR/WCAG21//) e o [Modelo de Acessibilidade em Governo Eletrônico (e-MAG)](https://emag.governoeletronico.gov.br/). Não beneficia apenas as pessoas com deficiências, mas também melhora a experiência do usuário para todos, incluindo idosos e todos aqueles que se encontram em ambientes com condições precárias de visualização ou navegação.

Reconhecemos que um *design* eficaz é aquele que permite que todos os usuários, independentemente de habilidades ou deficiências, possam interagir com os produtos digitais de maneira intuitiva e eficiente. Ao desenvolver o Design System do Governo do Brasil, uma atenção especial foi dada para assegurar que todos os artefatos disponibilizados estejam alinhados com os princípios de usabilidade e acessibilidade.

## O que são UIKits?

UI Kits, ou Kits de Interface do Usuário, são conjuntos de recursos e elementos pré-fabricados que são utilizados no design de interfaces digitais. Incluem uma variedade de elementos gráficos como botões, ícones, controles de navegação, tipografia, paletas de cores, e outros componentes de *design* que são necessários na criação de *websites*, aplicativos e sistemas.

> $link [*Downloads*](/addlink)
> O Padrão Digital de Governo conta com um amplo UIKit desenvolvido no **Figma** e no **Adobe XD** que podem ser facilmente baixados.

## Quem se beneficia com o Padrão Digital de Governo?

O uso do Padrão Digital de Governo beneficia uma ampla gama de *stakeholders* no processo de desenvolvimento e manutenção de produtos digitais, bem como os próprios usuários finais desses produtos.

Listamos a seguir alguns dos principais beneficiados:

### *Designers*

Os *designers* se beneficiam diretamente do uso do Padrão Digital de Governo, pois ele fornece um conjunto de diretrizes, padrões e UIKits reutilizáveis que garantem consistência e eficiência no *design*. Isso reduz o tempo necessário para reinventar soluções para problemas comuns e permite que os *designers* se concentrem na solução de novos desafios.

### Desenvolvedores

Para os desenvolvedores, o Padrão Digital de Governo funciona como uma fonte única de verdade para a implementação de interfaces de usuário. Isso facilita a manutenção e a escalabilidade do código, além de acelerar o processo de desenvolvimento, pois eles podem reutilizar código e ter uma referência clara para a implementação de elementos de *design*.

### Gerentes de produto

Os gerentes de produto se beneficiam da eficiência trazida pelo Padrão Digital de Governo, o que pode significar um tempo de lançamento mais rápido para novos produtos ou funcionalidades. Eles também têm a segurança de que o produto final será mais consistente e alinhado com as expectativas dos usuários e objetivos de negócios.

### Equipe de *marketing*

A consistência visual e de experiência do usuário promovida pelo Padrão Digital de Governo ajuda a fortalecer a identidade da marca, garantindo que a comunicação seja coerente em todas as plataformas digitais.

### Usuários finais

Os usuários dos produtos digitais são os maiores beneficiados, pois um *Design System* bem implementado resulta em uma experiência de usuário mais coesa, intuitiva e acessível. Isso significa menos confusão, menos erros e uma experiência mais agradável para todos.

### Organizações

No nível organizacional, o Padrão Digital de Governo pode levar a uma economia significativa de custos e recursos a longo prazo. Com componentes reutilizáveis e diretrizes claras, diminui-se a necessidade de retrabalho e os custos associados a inconsistências e erros de *design*.

### Equipe de suporte

Com produtos mais intuitivos e consistentes, a equipe de suporte pode esperar menos chamadas de usuários confusos ou frustrados, e quando surgem problemas, eles têm um entendimento mais claro dos sistemas para oferecer soluções rápidas.
