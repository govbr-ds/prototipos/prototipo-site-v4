# Fluxo de desenvolvimento

Se você chegou até aqui, é provável que esteja envolvido(a) com o desenvolvimento de *software* e esteja em busca de ferramentas, componentes e exemplos de uso para implementar seu produto.

Tenha em mente que o Design System foi criado de maneira colaborativa, com seu código fonte inteiramente aberto. Desta forma, sinta-se convidado a contribuir com seus próprios "*merge requests*" sempre que desejar.

Lembre-se que possuímos uma [comunidade](https://discord.com/invite/NkaVZERAT7) ativa e sempre pronta para ajudar, à qual você pode recorrer sempre que necessário!

## 1- Estudo preliminar

-   Inicie conhecendo o "coração" do Design System. Domine os [Princípios e Valores](/introducao/principios), além dos [Fundamentos Visuais](/fundamentos). Eles serão o alicerce de tudo que você projetar. Conheça-os profundamente e use-os como bússola em cada decisão.

-   Entenda detalhadamente como funciona cada [componente](/addlink) e cada [padrão](/padroes). Isso é fundamental para tomar decisões assertivas. Absorva o conhecimento e mantenha-o como uma ferramenta permanente de consulta.

> $assim-que-se-faz
> Explore a aba "Visão Geral" para uma compreensão geral de uso. Caso deseje ir além, consulte a aba "Saiba Mais".

Assim que se sentir seguro com esses conceitos, você estará pronto para avançar.

**Siga em frente com confiança!**

## 2- Instale os recursos disponíveis

Antes de começar tenha em mente que os recursos e ferramentas do Design System podem ser aplicados em sistemas que utilizam HTML, CSS e JavaScript, além de *frameworks* como **Angular**, **ReactJS** e **Vue.js**.

Para usar os recursos disponíveis no Design System, é necessário ter o [**Node.js**](https://nodejs.org), na versão mínima v18.13. Se você já usa outros sistemas que também dependem do Node, é recomendável usar uma ferramenta de gerenciamento de versões do Node, como o [nvm](https://github.com/nvm-sh/nvm), que é fácil de usar confiável.

### Aplicações HTML, JS e CSS

Em aplicações que fazem uso de **HTML**, **JS** e **CSS** é possível integrar os arquivos por meio de arquivos locais que podem ser enviados diretamente para sua infraestrutura.

Para instalar basta executar o seguinte comando na raiz do seu projeto:

```bash
npm install @govbr-ds/core
```

Sugerimos o uso do template inicial como referência.

Copie o código para um arquivo html e confira se os caminhos dos arquivos `rawline.css` e `core.min.css` estão corretos.

Antes de </body> temos a chamada para o arquivo `core.min.js`, confira se o caminho está correto de acordo com as pastas do seu projeto.

```html
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <!-- Font Rawline-->
    <link
      rel="stylesheet"
      href="https://cdngovbr-ds.estaleiro.serpro.gov.br/design-system/fonts/rawline/css/rawline.css"
    />
    <!-- Font Raleway-->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"
    />
    <!-- Design System GOV.BR CSS-->
    <link rel="stylesheet" href="node_modules/@govbr-ds/core/dist/core.min.css" />
    <!-- Fontawesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
    <title>Padrão Digital de Governo</title>
  </head>
  <body>
    <!-- Conteúdo-->
    <!-- Scripts de componentes -->
    <script type="module" src="<node_modules></node_modules>/@govbr-ds/core/dist/core.min.js"></script>
  </body>
</html>
```

### Aplicações ReactJS, Angular, Vue.js

Para aplicações que fazem uso de *frameworks* **SPA (Single-page application)**, sugerimos o uso dos [*Web Components*](https://www.webcomponents.org/specs), um conjunto de APIs de plataforma da *web* que permitem criar novas *tags* HTML personalizadas, reutilizáveis e encapsuladas para uso em páginas e aplicativos da *web*.

Considere um componente desenvolvido em React ou Angular. Utilizando *Web Components*, esse componente torna-se reutilizável em diversas aplicações, independentemente do *framework* adotado.

Essa abordagem simplifica a manutenção e atualização do código, eliminando a preocupação com a compatibilidade entre *frameworks*. Além disso, os *Web Components* oferecem um nível superior de segurança, graças ao encapsulamento da lógica e do estilo do componente em um DOM isolado.

Para instalar o pacote por meio da ferramenta **NPM** utilize o seguinte comando:

```bash
npm install --save @govbr-ds/webcomponents
```

Será necessário importar a estrutura básica em seu projeto:

```bash
@import "node_modules/@govbr-ds/webcomponents/dist/webcomponents.umd.min.js"
```

> $link-externo [*Web Components*](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc)
> Você pode encontrar mais informações em nosso GitLab.

**Agora você já está pronto para se familiarizar com os artefatos de desenvolvimentos utilizados no *Design System*!**

## 3- *Templates* e Componentes

Descubra os *templates* e componentes do Design System para reutilização e obtenha inspiração ao desenvolver seu projeto.

Familiarize-se com os artefatos do Design System para maximizar o uso do conteúdo disponível neste site.

### Utilitários

Facilitam o processo de criação de novas estruturas e promovem a consistência da aplicação através do reuso de classes CSS pré-definidas. Isso inclui *breakpoints*, *containers*, *grids*, colunas, espaçamentos e muitos outros.

> $assim-que-se-faz
> Você pode encontrar informações específicas sobre esse tema na aba **"Códigos e Utilitários"**, dentro de cada Fundamento Visual, Padrão ou Componente.

### Comportamentos JS

Classes JavaScript destinadas a adicionar funcionalidades específicas aos componentes. Entre elas, estão comportamentos como *Scrim*, *Accordion*, *Dropdown*, e muitos outros.

> $assim-que-se-faz
> Você pode encontrar informações específicas sobre esse tema na aba **"Códigos e Utilitários"**, dentro de cada Fundamento Visual, Padrão ou Componente.

### *Tokens de design*

Para facilitar a comunicação entre *designers* e desenvolvedores, utilizamos o conceito de ***tokens de design***. Isso torne mais fácil a integração do seu sistema, minimizando a necessidade de codificação CSS e assegurando a consistência visual entre as interfaces projetadas e a implementação de código.

> $nao-esqueca
> *Tokens de design* são variáveis semânticas que armazenam atributos visuais, como cores, tipografia, espaçamento entre outro, para garantir consistência e escalabilidade em um *Design System*.

Isso significa que, se houver mudanças nas interfaces, ajustes no CSS serão minimizados ou até mesmo desnecessários.

> $assim-que-se-faz
> Essas informações pode ser consultada em detalhe na aba "Tokens" das diretrizes.

Prefira sempre criar seus protótipos utilizando os *tokens*, isso facilitará futuras alterações visuais e mantém a consistência de seus *layouts* garantida.

### *Web Components*

São componentes reutilizáveis e independentes, compatíveis com qualquer aplicação *web*, incluindo Angular, ReactJS e Vue.js;

Para facilitar o entendimento de como consumir o projeto em tecnologias específicas, disponibilizamos um [*Quick Start*](/addlink) com exemplos de projetos utilizando *Web Components*.

> $link-externo [*Web Components*](https://webcomponent-ds.estaleiro.serpro.gov.br/?path=/story/introdu%C3%A7%C3%A3o--page)
> Confira nosso ***Story Book***.

## 4- Usabilidade e Acessibilidade

Os componentes do Design System são criados com foco em **usabilidade** e **acessibilidade**, contudo, para assegurar que seu projeto seja realmente eficaz nesses aspectos, é crucial entender bem esses conceitos e seguir as diretrizes recomendadas.

Para isso, contamos com profissionais preparados para cuidar tanto da usabilidade quanto da acessibilidade. Como o Design System se propõe a ser um produto em constante evolução, estamos sempre atualizando nossas diretrizes trazendo o que há de mais atualizado nesses quesitos.

> $assim-que-se-faz
> Sempre que possível, tenha o apoio de um designer para garantir que seu projeto atenda aos requisitos de usabilidade e acessibilidade.

> $nao-esqueca
> Recomendamos a utilização das orientações definidas nas nossas diretrizes. No entanto, é possível realizar alterações, desde que estejam em conformidade com regras básicas (Princípios e Fundamentos Visuais) e sejam justificadas por uma boa razão, como, por exemplo, a melhoria da experiência do usuário. Na dúvida, consulte um designer.

## 5- Release Notes

***Release notes*** (notas de lançamento) são documentos que acompanham uma nova versão de um *software*, detalhando as atualizações, melhorias, correções de erros e novos recursos implementados, além de informações relevantes para ajudar os usuários a entenderem as mudanças e como elas podem afetar a utilização do produto.

Eles são essenciais para a comunicação transparente entre o time do Design System e os usuários, facilitando o acompanhamento do progresso e das evoluções.

Você pode encontra-las no menu principal do *site* ou na nossa [comunidade](https://discord.com/invite/NkaVZERAT7).
