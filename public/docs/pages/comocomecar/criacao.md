# Fluxo de criação

Seja você um *designer*, um especialista em Experiência do Usuário (UX), conteudista ou alguém que deseja compreender as melhores práticas para trabalhar com um *Design System*, você está no lugar certo certo. Nós preparamos um guia passo a passo repleto de dicas que irão ajudá-lo a maximizar o uso de todos os recursos disponíveis no nosso Design System.

Lembre-se que possuímos uma [comunidade](https://discord.com/invite/NkaVZERAT7) ativa e sempre pronta para ajudar, à qual você pode recorrer sempre que necessário!

## 1- Estudo preliminar

-   Inicie conhecendo o "coração" do Design System.
Domine os [Princípios e Valores](/introducao/principios), além dos [Fundamentos Visuais](/fundamentos). Eles serão o alicerce de tudo que você projetar. Conheça-os profundamente e use-os como bússola em cada decisão de *design*.

-   Entenda detalhadamente como funciona cada [componente](/addlink) e cada [padrão](/padroes). Isso é fundamental para tomar decisões de *design*. Absorva o conhecimento e mantenha-o como uma ferramenta permanente de consulta.

> $assim-que-se-faz
> Explore a aba "Visão Geral" para uma compreensão geral de uso. Caso deseje ir além, consulte a aba "Saiba Mais".

Assim que se sentir seguro com esses conceitos, você estará pronto para avançar.

**Siga em frente com confiança!**

## 2- Prototipagem

Agora é hora de arregaçar as mangas e mergulhar no processo criativo, começando por um passo fundamental: **conhecer profundamente seus usuários**.

-   Faça pesquisas, dialogue, descubra o que precisam, o que os frustra e o que almejam;

-   Em seguida, é hora de visualizar suas ideias. Pegue um papel e comece a esboçar seu *layout*, refinando-o até que se aproxime da sua visão ideal;

-   Com um esboço em mãos, você está pronto para criar um protótipo digital. Utilize um programa de prototipagem da sua preferência. Para acelerar seu trabalho, oferecemos diversos **UIKits** para as ferramentas mais utilizadas no mercado.

> $porque-e-importante
> UIKit é uma coleção de componentes visuais prontos para uso que vão poupar seu tempo, pois você não precisará começar o trabalho do zero. Escolha os componentes necessários e incorpore-os ao seu *design* com facilidade.
> [Faça o *download*](/addlink) de nossos UIKits.

Também disponibilizamos **modelos** prontos, que são telas construídas com nossos UIKits, oferecendo exemplos concretos que você pode simplesmente usar ou adaptar conforme suas necessidades.

> $link [Modelos](/addlink)
> Faça o *download* de nossos modelos.

**Sinta-se à vontade para explorar e experimentar!**

### Ilustrações

Se seu projeto necessitar de ilustrações, confira nossos pacotes disponíveis que já seguem todas as recomendações descritas na [diretriz de Ilustração](/fundamentos/ilustracao). Você pode simplesmente utilizá-los ou modificá-los conforme suas necessidades.

> $link [Ilustrações](https://gitlab.com/govbr-ds/bibliotecas/govbr-ds-assets-de-design/-/tree/main/ilustracoes?ref_type=heads)
> Faça o *download* de nossos pacotes.

Nossas ilustrações são atualizadas pela própria [comunidade](https://discord.com/invite/NkaVZERAT7). Contribua!

### *Tokens de design*

Para facilitar a comunicação entre *designers* e desenvolvedores, utilizamos o conceito de ***tokens de design***.

> $nao-esqueca
> *Tokens de design* são variáveis semânticas que armazenam atributos visuais, como cores, tipografia, espaçamento entre outro, para garantir consistência e escalabilidade em um *Design System*.

Prefira sempre criar seus protótipos utilizando os *tokens*, isso facilitará futuras alterações visuais e mantém a consistência de seus *layouts* garantida.

> $assim-que-se-faz
> Essas informações pode ser consultada em detalhe na aba "Tokens" das diretrizes.

### *Writing*

Para facilitar a tarefa de criar o conteúdo textual, disponibilizamos uma seção com dicas focadas em *writing*. Você pode encontrar orientações para manter seu texto dentro dos padrões do Design System e inclusive contamos com um Dicionário de Vocabulário Controlado alimentado pela nossa [comunidade](https://discord.com/invite/NkaVZERAT7).

> $link [*Padrão writing*](/padroes/writing)
> Consulte as diretrizes para entender melhor.

## 3- Usabilidade e Acessibilidade

Os componentes do Design System são criados com foco em **usabilidade** e **acessibilidade**, contudo, para assegurar que seu projeto seja realmente eficaz nesses aspectos, é crucial entender bem esses conceitos e seguir as diretrizes recomendadas.

Para isso, contamos com profissionais preparados para cuidar tanto da usabilidade quanto da acessibilidade. Como o Design System se propõe a ser um produto em constante evolução, estamos sempre atualizando nossas diretrizes trazendo o que há de mais atualizado nesses quesitos.

> $assim-que-se-faz
> Se você não for um *designer*, é recomendável buscar aconselhamento de um especialista para tomar decisões que elevarão a qualidade do seu produto.

> $nao-esqueca
> Recomendamos a utilização das orientações definidas nas nossas diretrizes. No entanto, é possível realizar alterações, desde que estejam em conformidade com regras básicas (Princípios e Fundamentos Visuais) e sejam justificadas por uma boa razão, como, por exemplo, a melhoria da experiência do usuário.

## 4- *Release Notes*

***Release notes*** (notas de lançamento) são documentos que acompanham uma nova versão de um *software*, detalhando as atualizações, melhorias, correções de erros e novos recursos implementados, além de informações relevantes para ajudar os usuários a entenderem as mudanças e como elas podem afetar a utilização do produto.

Eles são essenciais para a comunicação transparente entre o time do Design System e os usuários, facilitando o acompanhamento do progresso e das evoluções.

Você pode encontra-las no menu principal do *site* ou na nossa [comunidade](https://discord.com/invite/NkaVZERAT7).
