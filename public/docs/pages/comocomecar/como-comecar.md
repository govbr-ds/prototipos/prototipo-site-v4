## Bem-vindo(a) ao Padrão Digital de Governo

O **Padrão Digital de Governo** (também chamado de [Design System do Governo do Brasil](/introducao/sobre)) é a sua bússola para navegar pelo universo do *design* de serviços públicos em todas as esferas (federal, estadual e municipal). Aqui, unimos eficiência, acessibilidade e estética para criar experiências digitais que transformam a interação do cidadão com o Estado.

**Iniciar é simples e transformador!**

<iframe width="560" height="315" src="https://www.youtube.com/embed/xHqm5nrQyV4?si=PWxiR__Dp-XzNR6L" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Por que usar um *Design System*?

*Design System* é uma coleção de padrões reutilizáveis e componentes guiados por padrões claros, que são usados para garantir a consistência e a eficiência de um produto ou serviço em múltiplas plataformas e dispositivos. Serve como uma única fonte de verdade para *designers*, desenvolvedores e todos os envolvidos no processo de desenvolvimento de um produto.

Principais razões para adotar um *Design System* são:

1. **Consistência:** assegura que todos os elementos de *design* e interações sejam consistentes em todas as telas e aplicativos. Isso não apenas reforça a identidade da marca, mas também melhora a experiência do usuário, pois os usuários aprendem padrões de interação que funcionam da mesma forma em diferentes contextos;

2. **Eficiência:** componentes reutilizáveis e padrões pré-definidos economizam tempo e recursos, pois reduzem a necessidade de iniciar do zero cada novo projeto;

3. **Colaboração:** facilita a colaboração entre equipes, oferecendo uma linguagem comum e recursos compartilhados, o que ajuda a evitar mal-entendidos e retrabalho;

4. **Escalabilidade:** é projetado para ser escalável, permitindo que novos componentes ou padrões sejam adicionados com facilidade à medida que o produto ou serviço cresce e evolui;

5. **Manutenção:** a manutenção do produto torna-se mais simples, pois as mudanças podem ser feitas de forma centralizada e distribuídas em todos os projetos de maneira controlada;

6. **Acessibilidade:** incorpora padrões de acessibilidade, garantindo que os produtos sejam utilizáveis por pessoas com diversas capacidades;

7. **Inovação:** com a operação diária facilitada pelo uso de padrões, as equipes podem dedicar mais tempo à inovação e ao aprimoramento do produto.

Em resumo, um *Design System* é um investimento na base de um produto digital, garantindo que ele seja construído sobre uma fundação sólida e coesa que oferece uma ótima experiência para o usuário e facilita o trabalho das equipes envolvidas.

### O que preciso saber antes de começar?

Antes de começar um projeto utilizando o Padrão Digital de Governo é importante estar alinhado com todo o conteúdo básico necessário para compreende o processo de trabalho. Que tal começar pelos indicados a seguir?

#### 1- Princípios do Padrão Digital de Governo

Tenha certeza de que compreendeu completamente os **Princípios** e **Valores** estabelecidos, pois eles são a base de todo o seu trabalho.

> $link [Princípios do Padrão Digital de Governo](/introducao/principios)
> Leia atentamente e compreenda os nossos princípios e valores.

#### 2- Fundamentos Visuais

Dominar os **Fundamentos Visuais** é o próximo passo garantir uma linguagem visual unificada. Incluem elementos essenciais padronizados, como cores, tipografias, espaçamentos, ícones, entre outros, para garantir consistência e clareza.

> $link [Fundamentos Visuais](/fundamentos)
> Esteja familiarizado com os conceitos básicos dos principais Fundamentos Visuais.

#### 3- Comunidade

Para manter todos os usuários mais próximos, criamos uma comunidade no **Discord**. Este é o espaço ideal para trocarmos informações, tirar dúvidas, sugerir melhorias ou simplesmente discutir assuntos relacionados ao Design System.

> $link-externo [Comunidade](https://discord.com/invite/NkaVZERAT7)
> Fique à vontade e junte-se a nós na comunidade do Design System.

**Agora você já está pronto para começar!**

Escolha o fluxo de trabalho mais adequado com seu perfil de atuação?

<div className="groupImage">
        <a href="/introducao/fluxo-criacao">
            <img src="imagens/banner-dsg.svg" alt="Quero trabalhar com criação" style="width: 100%;">
        </a>
        <a href="/introducao/fluxo-desenvolvimento">
            <img src="imagens/banner-dev.svg" alt="Quero trabalhar com desenvolvimento" style="width: 100%;">
        </a>
</div>
